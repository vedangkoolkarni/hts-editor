const { app, BrowserWindow, ipcMain } = require('electron')
let pids = []
var ps = require("process")
let mainWindow
app.on('ready', createWindow)
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})
app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
})

app.on('before-quit', function () {
    pids.forEach(function (pid) {
        // A simple pid lookup
        console.log("in kill block")
        console.log(pids)
        console.log(ps, "ps..")
        ps.kill(pid, function (err) {
            if (err) {
                throw new Error(err);
            }
            else {
                console.log('Process %s has been killed!', pid);
            }
        });
    });
    console.log("all kill and app close")
});
function createWindow() {
    mainWindow = new BrowserWindow(
        {
            width: 1366,
            height: 768,
            frame: true
        })
    mainWindow.maximize()
    mainWindow.loadFile('src/index.html')
    mainWindow.on('closed', function () {
        mainWindow = null
    })
    mainWindow.openDevTools()
}
