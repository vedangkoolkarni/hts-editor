#include "windowcontrol.h"
#include <nan.h>
#include<iostream>
#include<string>
#include <array>
#include<windows.h>
#include <sstream>
#include <algorithm>
#include <iterator>

using namespace std;
using v8::Context;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Value;
using v8::Array;
using v8::Exception;

#pragma comment(lib, "User32.lib")
Nan::Persistent<v8::Function> ControlWindow::constructor;


ControlWindow::ControlWindow(HWND handle) {
    this->windowHandle = handle;
}

ControlWindow::~ControlWindow() {}

void ControlWindow::Init(v8::Local<v8::Object> exports) {
    Nan::HandleScope scope;

    // Prepare constructor template
    v8::Local<v8::FunctionTemplate> tpl = Nan::New<v8::FunctionTemplate>(New);
    tpl->SetClassName(Nan::New("ControlWindow").ToLocalChecked());
    tpl->InstanceTemplate()->SetInternalFieldCount(1);

    Nan::SetPrototypeMethod(tpl, "GetActiveWindow", GetActiveWindow);
    Nan::SetPrototypeMethod(tpl, "GetWindowByClassName", GetWindowByClassName);
    Nan::SetPrototypeMethod(tpl, "getTitle", getTitle);
    Nan::SetPrototypeMethod(tpl, "minimize", minimize);
    Nan::SetPrototypeMethod(tpl, "GetWindowByTitleExact", GetWindowByTitleExact);
    Nan::SetPrototypeMethod(tpl, "setForegroundWindow", setForegroundWindow);
    Nan::SetPrototypeMethod(tpl, "move", move);
    Nan::SetPrototypeMethod(tpl, "startPreview", startPreview);
    Nan::SetPrototypeMethod(tpl, "setWindowPosition", setWindowPosition);

    constructor.Reset(tpl->GetFunction());
    exports->Set(Nan::New("ControlWindow").ToLocalChecked(), tpl->GetFunction());
}

void ControlWindow::New(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    if (info.IsConstructCall()) {
        ControlWindow *obj = new ControlWindow((HWND)info[0]->IntegerValue());
        obj->Wrap(info.This());
        info.GetReturnValue().Set(info.This());
    } else {
        const int argc = 1;
        v8::Local<v8::Value> argv[argc] = {info[0]};
        v8::Local<v8::Function> cons = Nan::New(constructor);
        info.GetReturnValue().Set(cons->NewInstance(argc, argv));
    }
}
void ControlWindow::GetActiveWindow(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    v8::Local<v8::Function> cons = Nan::New(constructor);
    const int argc = 1;
    HWND fgWin = GetForegroundWindow();
    v8::Local<v8::Value> argv[1] = {Nan::New((int)fgWin)};
    info.GetReturnValue().Set(Nan::NewInstance(cons, argc, argv).ToLocalChecked());
}
void ControlWindow::GetWindowByClassName(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    v8::Local<v8::Function> cons = Nan::New(constructor);

    v8::String::Utf8Value className(info[0]);
    std::string sClassName = std::string(*className);

    HWND fgWin = FindWindowEx(0, 0, sClassName.c_str(), 0);

    const int argc = 1;
    v8::Local<v8::Value> argv[1] = {Nan::New((int)fgWin)};
    info.GetReturnValue().Set(Nan::NewInstance(cons, argc, argv).ToLocalChecked());
}
void ControlWindow::getTitle(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    ControlWindow* obj = Nan::ObjectWrap::Unwrap<ControlWindow>(info.This());

    char wnd_title[256];
    GetWindowText(obj->windowHandle, wnd_title, sizeof(wnd_title));

    info.GetReturnValue().Set(Nan::New(wnd_title).ToLocalChecked()); 
}
void ControlWindow::GetWindowByTitleExact(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    v8::Local<v8::Function> cons = Nan::New(constructor);

    v8::String::Utf8Value exactTitle(info[0]);
    std::string sExactTitle = std::string(*exactTitle);

    HWND fgWin = FindWindow(NULL, sExactTitle.c_str());

    const int argc = 1;
    v8::Local<v8::Value> argv[1] = {Nan::New((int)fgWin)};
    info.GetReturnValue().Set(Nan::NewInstance(cons, argc, argv).ToLocalChecked());  
}
void ControlWindow::move(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    v8::Local<v8::Function> cons = Nan::New(constructor);
   ControlWindow* obj = Nan::ObjectWrap::Unwrap<ControlWindow>(info.This());

    const size_t x = info[0]->Int32Value();
    const size_t y = info[1]->Int32Value();
    const size_t w = info[2]->Int32Value();
    const size_t h = info[3]->Int32Value();

    MoveWindow(obj->windowHandle, x, y, w, h, true);
}
void ControlWindow::minimize(const Nan::FunctionCallbackInfo<v8::Value>& info) {
     v8::Local<v8::Function> cons = info[0].As<v8::Function>();
    ControlWindow* obj = Nan::ObjectWrap::Unwrap<ControlWindow>(info.This());
    PostMessage(obj->windowHandle, WM_SYSCOMMAND, SC_MINIMIZE, 0);
    //PostMessage(obj->windowHandle, WM_SYSCOMMAND, SC_RESTORE, 0);
    const int argc = 1;
    v8::Local<v8::Value> argv[1] = {Nan::New("minimizing").ToLocalChecked()};
    info.GetReturnValue().Set(Nan::NewInstance(cons, argc, argv).ToLocalChecked());  
    
}
void ControlWindow::setForegroundWindow(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    //v8::Local<v8::Function> cons = Nan::New(constructor);
    v8::Local<v8::Function> cons = info[0].As<v8::Function>();
    ControlWindow* obj = Nan::ObjectWrap::Unwrap<ControlWindow>(info.This());
    
    SetFocus(obj->windowHandle);
    EnableWindow(obj->windowHandle, TRUE);
    SetActiveWindow(obj->windowHandle);
    
    BringWindowToTop(obj->windowHandle);
    SetWindowPos(obj->windowHandle, HWND_TOPMOST, 0, 0, 568, 320, SWP_NOMOVE | SWP_NOSIZE);
    SetForegroundWindow(obj->windowHandle);
    // Set up a generic keyboard event.
    SetWindowLong(obj->windowHandle, GWL_STYLE, GetWindowLong(obj->windowHandle, GWL_STYLE) & ~WS_BORDER & ~WS_SIZEBOX);
    SetWindowPos(obj->windowHandle, HWND_TOPMOST, 100,100,568,320, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE); 
    ShowWindow(obj->windowHandle, SW_SHOW);       
    
    INPUT ip;
    ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0;
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;
    HKL kbl = GetKeyboardLayout(0);
        ip.ki.wVk = VkKeyScanExA('q', kbl); //<== don't mix ANSI with UNICODE
        ip.ki.dwFlags = 0; //<= Add this to indicate key-down
        SendInput(1, &ip, sizeof(ip));
        ip.ki.dwFlags = KEYEVENTF_KEYUP;
        SendInput(1, &ip, sizeof(ip));

    
    //SetWindowLong(obj->windowHandle, GWL_STYLE, 0 );  // Without 1 point border = white rectangle 
    //SetWindowLong(obj->windowHandle, GWL_EXSTYLE, WS_EX_CLIENTEDGE);  // Without 1 point border = white rectangle 
    
    const int argc = 1;
    v8::Local<v8::Value> argv[1] = {Nan::New("made to foreground").ToLocalChecked()};
    //info.GetReturnValue().Set(Nan::NewInstance(cons, argc, argv).ToLocalChecked());  
    Nan::MakeCallback(Nan::GetCurrentContext()->Global(), cons, argc, argv);
    
}
void ControlWindow::startPreview(const Nan::FunctionCallbackInfo<v8::Value>& info) {
   //v8::Local<v8::Function> cons = Nan::New(constructor);
   v8::Local<v8::Function> cons = info[0].As<v8::Function>();
    ControlWindow* obj = Nan::ObjectWrap::Unwrap<ControlWindow>(info.This());

    const int argc = 1;
    v8::Local<v8::Value> argv[1] = {Nan::New("startPreview").ToLocalChecked()};
    //info.GetReturnValue().Set(Nan::NewInstance(cons, argc, argv).ToLocalChecked());  
    Nan::MakeCallback(Nan::GetCurrentContext()->Global(), cons, argc, argv);
    
}
void ControlWindow::setWindowPosition(const Nan::FunctionCallbackInfo<v8::Value>& info) {
    v8::Local<v8::Function> cons = info[0].As<v8::Function>();
    ControlWindow* obj = Nan::ObjectWrap::Unwrap<ControlWindow>(info.This());

    //SetWindowLong(obj->windowHandle, GWL_STYLE, 0 );  // Without 1 point border = white rectangle 
    //SetWindowPos(obj->windowHandle, 0, 100,100,568,320, SWP_FRAMECHANGED); 


    const int argc = 1;
    v8::Local<v8::Value> argv[1] = {Nan::New("startPreview").ToLocalChecked()};
    Nan::MakeCallback(Nan::GetCurrentContext()->Global(), cons, argc, argv);  
}
/*void ControlWindow::New(const Nan::FunctionCallbackInfo<v8::Value>& info) {
   
}
void ControlWindow::New(const Nan::FunctionCallbackInfo<v8::Value>& info) {
   
}
void ControlWindow::New(const Nan::FunctionCallbackInfo<v8::Value>& info) {
   
}
void ControlWindow::New(const Nan::FunctionCallbackInfo<v8::Value>& info) {
   
}
void ControlWindow::New(const Nan::FunctionCallbackInfo<v8::Value>& info) {
   
}
void ControlWindow::New(const Nan::FunctionCallbackInfo<v8::Value>& info) {
   
}
void ControlWindow::New(const Nan::FunctionCallbackInfo<v8::Value>& info) {
   
}


*/
/**/