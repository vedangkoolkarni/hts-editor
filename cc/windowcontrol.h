#ifndef ControlWindow_H
#define ControlWindow_H

#include <nan.h>

class ControlWindow : public Nan::ObjectWrap {
 public:
  static void Init(v8::Local<v8::Object> exports);

  explicit ControlWindow(HWND handle);
  ~ControlWindow();

  	static void New(const Nan::FunctionCallbackInfo<v8::Value>& info);
  	static void GetActiveWindow(const Nan::FunctionCallbackInfo<v8::Value>& info);
  	static void GetWindowByClassName(const Nan::FunctionCallbackInfo<v8::Value>& info);
  	static void GetWindowByTitleExact(const Nan::FunctionCallbackInfo<v8::Value>& info);
  	static void getTitle(const Nan::FunctionCallbackInfo<v8::Value>& info);
  	static void move(const Nan::FunctionCallbackInfo<v8::Value>& info);
  	static void setForegroundWindow(const Nan::FunctionCallbackInfo<v8::Value>& info);
  	static void startPreview(const Nan::FunctionCallbackInfo<v8::Value>& info);
  	static void minimize(const Nan::FunctionCallbackInfo<v8::Value>& info);
  	static void setWindowPosition(const Nan::FunctionCallbackInfo<v8::Value>& info);
  	static Nan::Persistent<v8::Function> constructor;
	/*static void exists(const Nan::FunctionCallbackInfo<v8::Value>& info);
	static void isVisible(const Nan::FunctionCallbackInfo<v8::Value>& info);
	
	static void getHwnd(const Nan::FunctionCallbackInfo<v8::Value>& info);
	static void getClassName(const Nan::FunctionCallbackInfo<v8::Value>& info);
	static void getPid(const Nan::FunctionCallbackInfo<v8::Value>& info);

	static void getParent(const Nan::FunctionCallbackInfo<v8::Value>& info);
	static void getAncestor(const Nan::FunctionCallbackInfo<v8::Value>& info);
	static void getMonitor(const Nan::FunctionCallbackInfo<v8::Value>& info);

	
	static void setWindowPos(const Nan::FunctionCallbackInfo<v8::Value>& info);
	static void showWindow(const Nan::FunctionCallbackInfo<v8::Value>& info);
	static void move(const Nan::FunctionCallbackInfo<v8::Value>& info);
	static void moveRelative(const Nan::FunctionCallbackInfo<v8::Value>& info);
	static void dimensions(const Nan::FunctionCallbackInfo<v8::Value>& info);*/

	HWND windowHandle;
};

#endif
