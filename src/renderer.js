const { ipcRenderer, remote } = require("electron")
const { exec } = require('child_process');
var child_process = require('child_process');
const { Menu, MenuItem } = remote
const { dialog } = require("electron").remote
var fs = require('fs');
const electron = require("electron")
let counter = 0
let trackCount = 0;
var rulerJs = require("./assets/js/ruler.js")
var tracksJs = require('./assets/js/tracks.js')
var previewJs = require('./assets/js/preview.js')
var filesJs = require('./assets/js/files.js')
var editorJs = require('./assets/js/editor.js')
/*var robot = require("robotjs")
var htsApi = require('bindings')('htsapi')
var control = new htsApi.ControlWindow(0)*/
var trackFlag = false;
var win
var timer = [], timerFlag = true, f, nextAt
var start;
let previewTop, previewLeft, configFilePath
var configObj = undefined
var selectedCanvas
var pcount = 0;
var tracksWrapperScrollLeftOld = 0;
var tracksWrapperScrollTopOld = 0;
var rawconfigObj = {}

$(document).ready(function () {
    window.electron = electron
    setupMenu()
    loadStartScreen()
    electron.remote.getCurrentWindow().openDevTools()
    showInProperties('nothing')
})

//transparency Button
$(document).on('click', '#btn-transparency', function () {
    selectedCanvas = previewJs.getSelectedCanvas()
    if (selectedCanvas == undefined) {
        showInProperties('message', 'Please select element to add Transparency')
    }
    else {
        showInProperties('video-properties')
    }
});



$("#vRange").on("change", function () {
    var val = this.value;
    $('#vProgress').val(val);
});




/*$("#videoRange").on("change",function(){
	var val = this.value;
	$('#videoProgress').val(val);
});*/


$("#videoRange").on("input", function () {

    previewJs.seekFromSeekBar(this.value)
    $('#videoProgress').val(this.value);
});

$(document).on('click', '#play-video', function () {
    if ($('#play-video').attr('title') == 'Play') {
        var allFileLengths = tracksJs.getAllFileLengths()
        var areaSelectDivObj = rulerJs.getAreaSelectDivObj()
        var len = Object.keys(allFileLengths).length;
        console.log(len)
        if (len > 0) {
            $("#playim").attr("src", "img/pause.png");
            $("#play-video").attr("title", "Pause");
            previewJs.playFromCanvas(allFileLengths, areaSelectDivObj)
        }
    }
    else {
        var allFileLengths = tracksJs.getAllFileLengths()
        var len = Object.keys(allFileLengths).length;
        console.log(len)
        if (len > 0) {
            $("#playim").attr("src", "img/play.png");
            $("#play-video").attr("title", "Play");
            previewJs.pauseFromCanvas(allFileLengths)
        }
    }
    pcount++;
});
$(document).on('click', '#mute-video', function () {
    if ($('video').prop('muted')) {
        $('video').prop('muted', false);
        $("#mute-video").attr("title", "volume-on");
        $("#muteim").attr("src", "img/unmute.png");

    } else {
        $('video').prop('muted', true);
        $("#mute-video").attr("title", "Mute");
        $("#muteim").attr("src", "img/mute.png");
    }
});
$("#similarity-range-slider").on('input change', function () {
    $("#similarity-value").text((this.value) + "%")
    transparencySimilarity = this.value / 100;
    console.log(transparencySimilarity, ":", transparencyOpacity)
    updateTransparency()
})
$("#opacity-range-slider").on('input change', function () {
    $("#opacity-value").text((this.value) + "%")
    transparencyOpacity = ((this.value) / 100);
    console.log(transparencySimilarity, ":", transparencyOpacity)
    updateTransparency()
})
$(document).on('click', '#pause', function () {
    if ($(this).text() == 'Pause') {
        timerFlag = false
        //console.log(timer);
        for (var index in timer) {
            clearTimeout(timer[index]);
        }
        timer = [];
        //console.log(timer);
        $(this).text('Play')
    }
    else if ($(this).text() == 'Play') {
        timerFlag = true
        start = null;
        f()
        $(this).text('Pause')
    }
    /*	win.setForegroundWindow(function(res){
    
        });
        win.setForegroundWindow(function(res){
    
        });*/
    //robot.keyTap("space");	
})
$(document).on('click', '#play-preview', function () {
    let areaObj = rulerJs.getAreaSelectDivObj()
    console.log(areaObj)

    f = function () {
        if (!start) {
            start = new Date().getTime()
            nextAt = start
        }
        nextAt += 100
        var drift = (new Date().getTime() - start) % 100;
        if (timerFlag) {
            //console.log('came here')
            let left = parseInt($(".current-pin").css('left').split("px")[0]);
            left++
            $(".current-pin").css('left', left)
            timer.push(setTimeout(f, nextAt - new Date().getTime()));
        }

    };
    f()
    previewJs.startProcessing(areaObj, 'preview', function (resObj) {
        console.log('res obj :', resObj)
        if (resObj.stop == true) {
            console.log('stop fired')
            timerFlag = false
            for (var index in timer) {
                clearTimeout(timer[index])
            }
        }
        if (resObj.title != undefined) {
            console.log('preview started with title ', resObj.title)
            setTimeout(function () {
				/*win = control.GetWindowByTitleExact('previewwindow')
				console.log('window object title : ',win.getTitle())
				
				win.setForegroundWindow(function(res1){
					console.log(res1)
				});
				console.log('before moving',previewTop,' previewLeft',previewLeft)
				let h,w
				w = parseInt($('#preview').css('width').split('=')[0])
				h = parseInt($('#preview').css('height').split('=')[0])
				win.move(previewLeft,previewTop,w,h)
				robot.keyTap('down')*/
                $('.current-pin').css('left', '0')
                //win.setWindowPosition(100,100,568,320)
            }, 500)
        }
        if (resObj.data != undefined) {
            console.log('out : ', resObj.data)
        }
    })
})
$(document).on('click', '#minimize', function () {
	/*win.minimize(function(res){
		console.log(res)
	});*/
})
$(document).on('click', '#btn-add-image', function () {
    filesJs.addFile(function (filePath, fileId) {
        previewJs.addImage(filePath, 'external-file', function (res) {

        })
    })
})
$(document).on('click', '#btn-add-background', function () {
    filesJs.addFileBackground(function (filePath, fileId) {
    })
})
$(document).on('click', '#btn-remove-background', function () {
    $('#background').css('background-image', 'none');
    $('#background').css('background-size', '100% 100%');

})
$(document).on('click', "#btn-add-video", function () {
    // signature needs to change
    filesJs.addFile(function (filePath, fileId) {
        tracksJs.addTrack1(filePath, fileId, 'external-file')
        previewJs.addVideo({ path: filePath }, 'external-file', function (res) {

        })
    })
})
$(document).on('click', '#btn-add-text', function () {
    previewJs.addText(function (res) {

    })
})
$(document).on("click", "#btn-delete-file", function () {
    let source = $('.selected-file-row').text()
    filesJs.removeFiles(function (deletedIds) {
        tracksJs.removeTracks(deletedIds)
        previewJs.removeCanvas(deletedIds, source)
    })
})
$(document).on('click', '#cut-portion', function () {
    rulerJs.cutPortion(function () {
        console.log('completed')
    })
})

$(document).on('click', "#bring-on-top", function () {
    previewJs.bringOnTop(function (res) {

    })
})
$(document).on('click', "#bring-to-bottom", function () {
    previewJs.bringToBottom(function (res) {

    })
})
$(document).on('click', "#above", function () {
    previewJs.above(function (res) {

    })
})
$(document).on('click', "#below", function () {
    previewJs.below(function (res) {

    })
})
$(document).on('click', "#save", function () {
    startProcessing()
})
$(document).on("click", "canvas", function (ev) {
    //sets selectedCanvas and selectedCanvas variables
    previewJs.canvasClick(ev, function () {

    })
})
$(document).on("click", "#load-editor", function (ev) {
    loadEditor()
})
$(".files").on("click", ".row", function (ev) {
    filesJs.onFileClick(ev.target)
    selectedCanvas = undefined
    resetBorderProperties()
})
$(document).on('click', '#play-from-canvas', function (ev) {
    var allFileLengths = tracksJs.getAllFileLengths()
    var areaSelectDivObj = rulerJs.getAreaSelectDivObj()
    previewJs.playFromCanvas(allFileLengths, areaSelectDivObj)
})
$(document).on('click', '#pause-from-canvas', function (ev) {
    var allFileLengths = tracksJs.getAllFileLengths()
    previewJs.pauseFromCanvas(allFileLengths)
})
$(document).on('click', '.a-sec', function (ev) {
    var trackContentScrollLeft = $('.track-content-wrap').scrollLeft();
    $('.current-pin').css('left', this.offsetLeft - trackContentScrollLeft);
    console.log('seeked');
    /*previewJs.pauseAllVideos()*/
    // var allFileLengths = tracksJs.getAllFileLengths()
    // previewJs.seekFromRuler(allFileLengths)
})
$(document).on('click', '#btn-border', function () {
    //pass id of wrapper to show in properties section
    selectedCanvas = previewJs.getSelectedCanvas()
    if (selectedCanvas == undefined) {
        showInProperties('message', 'Please select element to add Border')
    }
    else {
        showInProperties('border-properties')
    }
})
$(document).on('input change', '.b-p-row-input', function (ev) {
    updateBorder(this.getAttribute('border-type'), this.value)
})
$(document).on('click', '#cancel-Btn-Swal', function () {
    console.log("swal call")
    swal("Do you want to cancel processing?", {
        buttons: {
            cancel: "NO",
            catch: {
                text: "Yes",
                value: "Yes"
            }
        },
    }).then((value) => {
        console.log(value);
        switch (value) {
            case "Yes":
                swal("Processing Cancelled", "successfully", "success");
                jQuery("#saveModal").modal("hide");
                console.log("close call");
                console.log(configFilePath + 'settings.config', 'configFilePath..')
                editorJs.killChildProcess();
                //to load the configObj value after cancel processing  
				/*configObj = JSON.parse(fs.readFileSync(configFilePath+'settings.config', 'utf8'));
				handleMultiConfig(configObj)
				status = filesJs.testIntegrity(configFilePath, configObj, function (a, b, c) {
				})*/
				/*for(let i in rawconfigObj)
				{
					configObj[i]=rawconfigObj[i]
				}*/
                configObj = JSON.parse(JSON.stringify(rawconfigObj));
                console.log(configObj, rawconfigObj)
                break;
        }
    });
})
$('.track-content-wrap').on('scroll', function (event) {
    $('.tracks').scrollTop(this.scrollTop);
    $('.ruler').scrollLeft(this.scrollLeft);
})
function loadStartScreen() {
    $('#load-editor').click()
}
function loadEditor() {

    $('.start-screen-wrap').hide()
    $('.editor-wrap').show()

    previewLeft = electron.screen.getPrimaryDisplay().workArea.width * 0.34
    previewTop = electron.screen.getPrimaryDisplay().bounds.height - electron.screen.getPrimaryDisplay().workArea.height
    //console.log('preview width',)
    let h = (9 * parseInt($('#preview').css('width').split('px')[0])) / 16
    $('.container1').css('height', h)
    //console.log('fow width : ',$('#preview').css('width'),' height should be',h)
    electron.remote.getCurrentWindow().maximize()
    //electron.remote.getCurrentWindow().openDevTools()
    //60px is height of header
    rulerJs.drawRuler()
    rulerJs.handlePinMotion()
    $(".plugin-content").css('height', (electron.remote.getCurrentWindow().getSize()[1]) - 60)

    //console.log(electron.remote.getCurrentWindow().getBounds())
}
function setupMenu() {
    electron.remote.getCurrentWindow().setMenu(null)
    const menu = Menu.buildFromTemplate(getTemplate())
    electron.remote.getCurrentWindow().setMenu(menu)
}
function getTemplate() {
    const template =
        [
            {
                label: 'File',
                submenu: [
                    { label: 'Load Config', accelerator: 'CommandOrControl+L', click() { loadConfig() } },
                ]
            }
        ]
    return template
}
function handleMultiConfig(newConfigObj) {
    if (configObj == undefined) {
        configObj = newConfigObj
        for (let i = 0; i < configObj.finalMerger.length; i++) {
            configObj.finalMerger[i].transparency = {
                transparencyCheck: false
            }

        }
    }
    else {
        for (let i = 0; i < newConfigObj.length; i++) {
            configObj.finalMerger.push(newConfigObj.finalMerger[i])

        }
    }

    rawconfigObj = JSON.parse(JSON.stringify(configObj));
}

function createTracks(filePath, fileId, type, configFilePath, img, trackLeft) {

    let width = (trackLeft.end - trackLeft.start) * 10;

    var waveArgs = [];
    var outputFile = "track-" + (trackCount) + ".png"
    configObj.finalMerger[trackCount++].waveform = img;
    waveArgs = [
        'ffmpeg', '-i', filePath,
        '-filter_complex', 'showwavespic=s=' + width + 'x70:colors=#ffffff', '-frames:v', '1', '-y', configFilePath + 'tracks/' + outputFile
    ];

    img = 'tracks/' + outputFile;

    var cmdC = waveArgs.shift();
    var procO = child_process.spawn(cmdC, waveArgs);
    ipcRenderer.send('process_spawn', procO.pid);
    procO.on('close', function (data) {
        tracksJs.addTrack(filePath, fileId, type, configFilePath, img, trackLeft)
    })

};


function loadConfig() {

    filesJs.loadConfig(function (status, configObject_x, configFilePath_x) {
        // console.log(status)
        if (status != 404) {
            handleMultiConfig(configObject_x)//this function sets comnfig object

            configFilePath = configFilePath_x.replace('settings.config', '')
            console.log('new configObj : ', configObject_x)

            if (!fs.existsSync(configFilePath + '/tracks')) {
                fs.mkdirSync(configFilePath + '/tracks');
            }

            if (!fs.existsSync(configFilePath + '/output')) {
                fs.mkdirSync(configFilePath + '/output');
            }

            filesJs.addConfigFiles(configObj, configFilePath, function (filePath, fileId, type, img, trackLeft, windowCoords) {
                // In case of both path is an array of 0- camera , 1-screen
                // console.log('deciding whether to add tracks or not', img);

                if (img == undefined) {
                    trackFlag = true;
                }
                else {
                    trackFlag = false;
                }

                if (type == 'both') {
                    previewJs.addVideo({ path: [configFilePath + filePath[1]], windowCoords: windowCoords }, type + '-screen', function (res) {
                    })
                    previewJs.addVideo({ path: [configFilePath + filePath[0]] }, type + '-camera', function (res) {
                    })

                    if (trackFlag) {
                        createTracks([configFilePath + filePath[0]], fileId, type, configFilePath, img, trackLeft)
                    }
                    else {
                        tracksJs.addTrack([configFilePath + filePath[0]], fileId, type, configFilePath, img, trackLeft)
                    }

                }
                else {
                    previewJs.addVideo({ path: [configFilePath + filePath], windowCoords: windowCoords }, type, function (res) {
                    })

                    if (trackFlag) {
                        createTracks([configFilePath + filePath], fileId, type, configFilePath, img, trackLeft)
                    }
                    else {
                        tracksJs.addTrack([configFilePath + filePath], fileId, type, configFilePath, img, trackLeft)
                    }
                }

            })
        }
    })
};


function startProcessing() {
    var canvases = previewJs.getCanvases()
    var areaObj = rulerJs.getAreaSelectDivObj()
    console.log(canvases)
    if (canvases.length > 0) {
        console.log(configObj, rawconfigObj);
        configObj = JSON.parse(JSON.stringify(rawconfigObj));
        editorJs.preprocess(configObj, configFilePath, areaObj, canvases)
    }
}
function showInProperties(id, message) {
    //console.log(id,message)
    $('#properties').children().each(function (ind, elem) {
        // console.log('this id : ', this.id, 'passes id : ', id, 'elem : ', elem)
        if (this.id != id)
            this.style.display = 'none'
        else if (this.id == id) {
            this.style.display = 'block'
        }
        if (id == 'message') {
            $('.message').show()
            $('.message').text(message)
        }
    })
}
function updateTransparency() {

    selectedCanvas = previewJs.getSelectedCanvas()
    //console.log('selected canvas : ',$(selectedCanvas).parent().get(0).id)
    //console.log('selected canvas : ',$(selectedCanvas).children().get(0).id)
    canvas_id = selectedCanvas.id;
    console.log('canvas_id', canvas_id, 'selectedCanvas', selectedCanvas)
    if (selectedCanvas != undefined && selectedCanvas.style.display != 'none') {
        let i = parseInt(canvas_id.charAt(7))
        i--;


        var colorHex = $('#hex-color').val();
        var transparencySimilarity = $('#similarity-range-slider').val() / 100;
        var transparencyOpacity = $('#opacity-range-slider').val() / 100;

        if (colorHex != '') {

            console.log(configObj.finalMerger[i])
            configObj.finalMerger[i].transparency = {
                transparencyCheck: true,
                colorHex: colorHex,
                transparencySimilarity: transparencySimilarity,
                transparencyOpacity: transparencyOpacity
            }

        }
        else {
            alert('Please Enter color value.')
        }
        console.log(configObj.finalMerger[0])
        rawconfigObj = JSON.parse(JSON.stringify(configObj));

    }
}
function updateBorder(borderType, borderWidth) {



    selectedCanvas = previewJs.getSelectedCanvas()
    // returns parent of selected canvas
    console.log('selected canvas : ', selectedCanvas)
    var borderColor = $('.b-p-row-input-color').val()
    if (checkBorderColor(borderColor)) {

        if (!borderColor.includes('#')) {
            borderColor = '#' + borderColor
        }
        console.log(borderType, borderWidth, borderColor)
        if (selectedCanvas != undefined && selectedCanvas.style.display != 'none') {
            switch (borderType) {
                case 'top':
                    selectedCanvas.style.borderTop = borderWidth + 'px solid ' + borderColor
                    selectedCanvas.setAttribute('borderTop', borderWidth)
                    break
                case 'right':
                    selectedCanvas.style.borderRight = borderWidth + 'px solid ' + borderColor
                    selectedCanvas.setAttribute('borderRight', borderWidth)
                    break
                case 'bottom':
                    selectedCanvas.style.borderBottom = borderWidth + 'px solid ' + borderColor
                    selectedCanvas.setAttribute('borderBottom', borderWidth)
                    break
                case 'left':
                    selectedCanvas.style.borderLeft = borderWidth + 'px solid ' + borderColor
                    selectedCanvas.setAttribute('borderLeft', borderWidth)
                    break
            }
            selectedCanvas.setAttribute('borderColor', borderColor)


        }
    }
}
function checkBorderColor(color) {
    if (color.length != 0) {
        return true
    }
    else {
        return false
    }
}
function resetBorderProperties() {
    $('.b-p-row-input-color').val('')
    $('.b-p-row-input').each(function () {
        this.value = 0
    })
}
// Mute code @author - Girish
$(document).on('click', '#mute-portion', function () {

    rulerJs.mutePortion(selectedTrackId, function () {

    })
})

$(document).on('click', '#process', function () {
    var fileResult = filesJs.getTheFiles()
    console.log(fileResult)
    rulerJs.process(fileResult, function () {

    })
})


function trackClick(ev) {
    selectedTrack = $(ev.currentTarget).attr('path')
    selectedTrackId = $(ev.currentTarget).attr('id')
    console.log(selectedTrack)
}
function trackContentClick(ev) {

    console.log(ev)
    selectedTrackContentId = $(ev.target).attr('id')
    console.log(selectedTrackContentId)
    rulerJs.getTrackContentID(selectedTrackContentId)
    console.log(selectedTrackContentId)
}
function RangeClick(ev) {
    console.log(ev)
    selectedRange = ev.target.id
    console.log(selectedRange)

}
$(document).on("click", ".vRange", function (ev) {
    RangeClick(ev)
})


$(document).on("click", ".a-track", function (ev) {
    trackClick(ev)
})
$(document).on("click", ".track-content", function (ev) {
    $('#mute-portion').css('display', 'inline')
    console.log("asdasdas")
    trackContentClick(ev)

})

$(document).on('change', '.vRange', function (ev) {
    console.log(ev);
    var id = ev.target.id.split('Range')[1];
    var val = this.value;

    $('.vProgress' + id).val(val);
    $('#volume' + id).html(val);
    console.log("volume is:-" + val)

});
