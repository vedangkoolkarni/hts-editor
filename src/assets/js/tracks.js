var child_process = require('child_process');
var maxTrackWidth = 0;
var aTrackName = `<div class="track-name" id="track_name_id_here">
                        <div id = "volume_seek_bar">
                            <progress id="vProgress" class="progress_here" value="100" max="100" ></progress>
                            <input type="range" id="Range_here" class="vRange"value="100" max="100" step="1" >
                        </div>
                        <div class="volume_class"id="volume_here">100</div><br>
                        <div class="track-name-inner">track_name_here</div>
                        <br>
                    </div>`;
var aTrackContent = `<div class="track-content" id="track_content_id_here">
                        <div id="wave_wrap_id_here" class="wave-wrap" style="background-image: url(\'url_here\');background-repeat:repeat;height:100%;width:width_here;left: left_here;">
                            <div id="track_inner_id_here" style="width:width_here;height:100%;background:rgba(0,0,0,0.5);">
                            </div>
                        </div>
                    </div>`;
var RangeCounter = 1
var rulerJs = require('./ruler.js');
var allFileLengths = {}

function addTrack1(filePath, fileId) {
    RangeId = "Range" + RangeCounter;
    progressId = "vProgress" + RangeCounter;
    volumeId = "volume" + RangeCounter;

    var trackContentId = "track-content-" + RangeCounter;
    var trackNameId = "track-name-" + RangeCounter;
    var innerTrackContentId = "track-inner-content-" + RangeCounter;

    let thisTrackName = aTrackName.replace('track_name_id_here', 'track-name-' + fileId.replace('file', 'track'))
    thisTrackName = thisTrackName.replace('track_name_here', 'external' + ' track')
    thisTrackName = thisTrackName.replace('track_type_here', 'external')
    thisTrackName = thisTrackName.replace('Range_here', RangeId)
    thisTrackName = thisTrackName.replace('progress_here', progressId)
    thisTrackName = thisTrackName.replace('volume_here', volumeId)
    thisTrackName = thisTrackName.replace('repeat_status', 'no-repeat')

    let thisTrackContent = aTrackContent.replace('track_content_id_here', 'track-content-' + fileId.replace('file', 'track-content'))
    thisTrackContent = thisTrackContent.replace('left_here', '0px')
    thisTrackContent = thisTrackContent.replace('track_inner_id_here', innerTrackContentId)

    var v = document.createElement("video")
    v.id = "myVideo"
    v.src = filePath[0]
    v.load()

    v.onloadeddata = function () {
        width = parseInt(this.duration) * 10
        var cropArgs = [];
        var outputFile = "output" + RangeCounter + ".png"
        cropArgs = [
            'ffmpeg', '-i', filePath[0],
            '-filter_complex',
            'showwavespic=s=' + width + 'x70:colors=#ffffff',
            '-frames:v',
            '1',
            '-y',
            './src/assets/' + outputFile
            //volume=enable=\'between(t,5,35)\':volume=0
        ];
        console.log(cropArgs)
        var cmdC = cropArgs.shift();
        var procO = child_process.spawn(cmdC, cropArgs);

        procO.stderr.on('data', function (data) {
            //console.log("sucess", data.toString());
        });
        procO.on('close', function (data) {
            var rulerWidth = $('.rule').width();
            console.log('current width : ', width, 'ruler width : ', rulerWidth);

            if (rulerWidth < width) {
                rulerJs.setRulerWidth(width - rulerWidth);
            }
            thisTrackContent = thisTrackContent.replace('url_here', "assets/" + outputFile)
            thisTrackContent = thisTrackContent.replace('width_here', width + "px")

            $('.track-name-wrap').append(thisTrackName);
            $('.track-content-inner-wrap').append(thisTrackContent);

            let x1 = $('#' + innerTrackContentId).get(0)
            $(x1).resizable({
                axis: 'x',
                containment: '#' + trackContentId,
                minWidth: 100,
                handles: 'e',
            })
            $(x1).draggable({
                axis: 'x',
                containment: '#' + trackContentId,
            })
        });
        console.log(v)
        $(v).remove();
    }

    //thisTrack = thisTrack.replace('url_here',bg)

    //thisTrack = thisTrack.replace('track_no_here',$('.tracks').children().length + 1)

    /*setTimeout(function(){
        //setupWaveForms($('#'+(fileId.replace('file','track'))),filePath)
        trackCounter++
    },1000)*/
    RangeCounter++
}

function addTrack(filePath, fileId, type, configFilePath_x, img, trackLeft) {
    var configFilePath = configFilePath_x.replace(/\\/g, '/');
    RangeId = "Range" + RangeCounter;
    progressId = "vProgress" + RangeCounter;
    volumeId = "volume" + RangeCounter;

    // var trackContentId = "track-content-" + RangeCounter;
    var trackNameId = "track-name-" + RangeCounter;
    var innerTrackContentId = "track-inner-content-" + RangeCounter;
    var waveWrapId = "wave-wrap-" + RangeCounter;

    let thisTrackName = aTrackName.replace('track_name_id_here', 'track-name-' + fileId.replace('file', 'track'))
    thisTrackName = thisTrackName.replace('track_name_here', type + ' track')
    thisTrackName = thisTrackName.replace('track_type_here', type)
    thisTrackName = thisTrackName.replace('Range_here', RangeId)
    thisTrackName = thisTrackName.replace('progress_here', progressId)
    thisTrackName = thisTrackName.replace('volume_here', volumeId)
    thisTrackName = thisTrackName.replace('repeat_status', 'no-repeat')

    var trackContentId = 'track-content-' + fileId.replace('file', 'track-content');
    let thisTrackContent = aTrackContent.replace('track_content_id_here', trackContentId);
    thisTrackContent = thisTrackContent.replace(/width_here/g, (trackLeft.end - trackLeft.start) * 10 + "px")
    thisTrackContent = thisTrackContent.replace(/left_here/g, (trackLeft.start * 10) + 'px')
    thisTrackContent = thisTrackContent.replace('track_inner_id_here', innerTrackContentId)
    thisTrackContent = thisTrackContent.replace('wave_wrap_id_here', waveWrapId)
    thisTrackContent = thisTrackContent.replace('url_here', configFilePath + '/' + img)

    //! Now we have two variables 1) thisTrackContent 2) thisTrackName
    // TODO  append thisTrackContent to track-content-wrap
    // TODO  append thisTrackName    to track-name-wrap
    $('.track-name-wrap').append(thisTrackName);
    $('.track-content-inner-wrap').append(thisTrackContent);
    //$('.tracks').append(thisTrack)
    RangeCounter++
    let content = $('#' + innerTrackContentId).get(0);
    let contentWrap = $('#' + waveWrapId).get(0);
    $(content).resizable({
        axis: 'x',
        handles: 'e,w',
        maxWidth: (trackLeft.end - trackLeft.start) * 10,
        containment: contentWrap,
        minWidth: 10,
        resize: function (e, ui) {
            trackInnerResized(e, ui);
        }
    })
    $(contentWrap).draggable({
        axis: 'x',
        containment: '#' + trackContentId,
        drag: function (ev) {
            trackOuterDragged(ev);
        }
    });
    console.log('containment set as : ', trackContentId)
    var obj = {
        type: type,
        left: (trackLeft.start * 10),
        right: (trackLeft.end * 10),
        innerLeft: 0,
        innerWidth: 0
    }
    allFileLengths[fileId.replace('file', 'track')] = obj
    setMaxTrackWidth(obj.right);
}
function removeTracks(deleteIds) {
    jQuery.each(deleteIds, function (ind, item) {
        item = item.replace('file', 'track')
        $('#' + item).remove()
    })
}
function getAllFileLengths() {
    return allFileLengths
}
function setMaxTrackWidth(width) {
    // console.log('came to set max width of ruler', maxTrackWidth, width);
    var rulerWidth = $('.rule').width();
    if (maxTrackWidth <= width) {
        maxTrackWidth = width;
        rulerJs.setRulerWidth(maxTrackWidth - rulerWidth);
    }
}
// ! When inner is resized 
// ? skip initial few seconds (innerLeft / 10)
// ! When outer is dragged
// ? file should start after that many few seconds (left / 10)
function trackInnerResized(eve, ui) {
    var newWidth = parseInt(eve.target.style.width.split('px')[0]);
    var newLeft = parseInt(eve.target.style.left.split('px')[0]);
    // should split track and number (e.g. track-1 should output ids[0] = track and ids[1] = 1)
    var ids = eve.target.id.split('-');
    var key = (ids[0] + '-' + ids[ids.length - 1]);
    var oldWidth = allFileLengths[key];
    console.log('newWidth : ', newWidth, 'newLeft : ', newLeft)
    // ? updating innerLeft
    allFileLengths[key].innerLeft = newLeft;
    allFileLengths[key].innerWidth = newWidth;
}
function trackOuterDragged(eve) {
    var newWidth = parseInt(eve.target.style.width.split('px')[0]);
    // should split track and number (e.g. track-1 should output ids[0] = track and ids[1] = 1)
    var ids = eve.target.id.split('-');
    var key = (ids[0] + '-' + ids[ids.length - 1]);
    var oldWidth = allFileLengths[key];
    console.log(newWidth, oldWidth);
}
$(document).on('change', '.checkbox', function (ev) {
    var id = ev.target.id.split('checkbox')[1];
    console.log(id)
    $("#track-content" + id).css('background-repeat', 'repeat')
});
module.exports = {
    addTrack: addTrack,
    addTrack1: addTrack1,
    removeTracks: removeTracks,
    getAllFileLengths: getAllFileLengths
};