const { ipcRenderer, remote } = require("electron")
const { dialog } = require("electron").remote
const electron = require("electron")
var fs = require('fs')
var path = require('path')
//var child_process = require('child_process');

let filePath;
var fileCounter = 1
var filepath
var toAppend
//var $ = require('jquery');
var this_row
var theFiles = {}
//var trackLeft = 0
var a_file_row = "<li class='row' id='file-id_here' data-file-type='file_type_here' data-file-path='file_path_here' style=background-color:rgb(31,36,42);>filename_here</li>";
var videoDurationsCounter = 0

function getTheFiles() {
    return theFiles
}
function addFile(cb) {
    dialog.showOpenDialog({
        browserWindow: electron.remote.getCurrentWindow(),
        title: 'Select File',
        buttonLabel: 'Select A File',
        filters: [
            { name: 'File', extensions: ['mp4', 'mkv', 'mov', 'mts', 'avi'] }
        ],
        properties: ['openFile']
    }, function (filename) {
        if (filename != undefined) {
            filePath = filename
            filename = filename[0]
            filepath = filename
            filename = filename.replace(/^.*[\\\/]/, '');

            if (filename != undefined) {
                this_row = a_file_row
                if (filename.length >= 24) {
                    filename = filename.substring(0, 24) + "...";
                    //console.log(filename,"if part")
                }
                this_row = this_row.replace("filename_here", filename)
                this_row = this_row.replace("id_here", fileCounter)
                this_row = this_row.replace("file_type_here", 'external-file')
                $(".files").append(this_row)

                theFiles["file-" + fileCounter] = { filePath: filePath }
                cb(filePath, "file-" + fileCounter)
                fileCounter++
            }
        }
    })
}
function addFileBackground(cb) {
    dialog.showOpenDialog({
        browserWindow: electron.remote.getCurrentWindow(),
        title: 'Select File',
        buttonLabel: 'Select File',
        filters: [
            { name: 'File', extensions: ['jpg', 'png'] }
        ],
        properties: ['openFile']
    }, function (filename) {
        if (filename != undefined) {
            filePath = filename
            filename = filename[0]
            filepath = filename
            filename = filename.replace((/\\/g), '/');
            filename = filename.replace('%20', ' ');
            //console.log(filename,"after replace path");
            $('#background').css('background-image', 'url("' + filename + '")');
            $('#background').css('background-size', '100% 100%');
            $('#background').css('background-repeat', 'no-repeat');
        }
    })
}

function removeFiles(cb) {
    selected = $('.selected-file-row').length
    let count = 0
    let deletedIds = []
    $('.selected-file-row').each(function (ind, html) {
        deletedIds.push(this.id)
        delete theFiles[this.id]
        $(this).remove()
        count++
        if (count == selected)
            cb(deletedIds)
    })
}
function onFileClick(el) {
    var id = el.id.replace('file', 'canvas')
    var fileType = el.getAttribute('data-file-type')
    hideMergerTypeCanvases()

    if (fileType == 'both') {
        $('#' + id + '-1').parent().show()
        $('#' + id + '-2').parent().show()
        var background = {
            width: parseInt($('#background').css('width').split('px')[0]),
            height: parseInt($('#background').css('height').split('px')[0]),
            left: (parseInt($('#tab').css('width').split('px')[0]) + parseInt($('#file-lists').css('width').split('px')[0]))
        }
    }
    else {
        $('#' + id).parent().show()
    }

    $('.row').removeClass('selected-file-row')
    $('.row').css('background-color', '#1f242a');
    if ($(el).hasClass("row selected-file-row")) {
        $(el).removeClass("selected-row");
        $(el).css('background-color', '#1f242a');
    }
    else {
        $(el).addClass("selected-file-row");
        $(el).css('background-color', '#3AB0FA');
    }
}
function hideMergerTypeCanvases() {
    $('.canvas').each(function () {
        var canvas_file_type = this.getAttribute('canvas_file_type')
        if (canvas_file_type != 'external-file') {
            $(this).parent().hide()
        }
    })
}
/*
*	This function cannot set startTime endTime properties because of asyn nature;
*	Handled in gotVideoDuration
*/
function getBlobDuration(obj, i, configFolderPath, type, cb) {
    const tempVideoEl = document.createElement('video')
    var dur;

    tempVideoEl.addEventListener('loadedmetadata', () => {
        //console.log('came here',tempVideoEl.duration)
        if (tempVideoEl.duration === Infinity) {
            tempVideoEl.currentTime = Number.MAX_SAFE_INTEGER

            tempVideoEl.ontimeupdate = () => {
                tempVideoEl.ontimeupdate = null
                //console.log(obj.finalMerger[i].type,'->',tempVideoEl.duration)
                obj.finalMerger[i].tempDur = tempVideoEl.duration
                tempVideoEl.currentTime = 0
                gotVideoDurations(cb, obj, configFolderPath)
            }
        }
        // Normal behavior
        else {
            //console.log(obj.finalMerger[i].type,'->',tempVideoEl.duration)
            obj.finalMerger[i].tempDur = tempVideoEl.duration
            gotVideoDurations(cb, obj, configFolderPath)
        }
    }, obj, i, type)
    if (type == 'camera' || type == 'screen' || type == 'both-camera') {
        tempVideoEl.src = configFolderPath + obj.finalMerger[i].cpath;
    }
    else if (type == 'both-screen') {
        tempVideoEl.src = configFolderPath + obj.finalMerger[i].spath;
    }

    //return durationP
}
function getLengthOfFinalMerger(finalMerger) {
    var len = 0
    for (var i = 0; i < finalMerger.length; i++) {
        if (finalMerger[i].type == 'camera' || finalMerger[i].type == 'screen')
            len++
        else if (finalMerger[i].type == 'both') {
            len += 2
        }
    }
    return len
}
/* will need a status here too*/
function gotVideoDurations(cbFromTestIntegrity, configObj, configFolderPath) {
    /*This function is called async*/
    var total = getLengthOfFinalMerger(configObj.finalMerger)
    videoDurationsCounter++
    if (total == videoDurationsCounter) {
	    /*args are : 0-status,1-configObject,2-configFolderPath
		*
		*	iterate over all objects and set duration as object with startTime and endTime
		*/
        for (var i = 0; i < configObj.finalMerger.length; i++) {
            var dur = configObj.finalMerger[i].tempDur
            delete configObj.finalMerger[i].tempDur
            configObj.finalMerger[i].duration = {}
            if (i == 0) {
                configObj.finalMerger[i].duration.startTime = 0
                configObj.finalMerger[i].duration.endTime = parseInt(dur)
            }
            else {
                configObj.finalMerger[i].duration.startTime = configObj.finalMerger[i - 1].duration.endTime
                configObj.finalMerger[i].duration.endTime = parseInt(configObj.finalMerger[i - 1].duration.endTime + dur)
            }
        }
        cbFromTestIntegrity(1, configObj, configFolderPath)
    }
}
function setVideoDuration(obj, type, configFolderPath, i, cb) {
    /*change this*/
    //if(obj.finalMerger[i].startTime != undefined)
    {
        if (type == 'camera' || type == 'screen') {
            getBlobDuration(obj, i, configFolderPath, type, cb)
        }
        else {
            getBlobDuration(obj, i, configFolderPath, 'both-camera', cb)
            getBlobDuration(obj, i, configFolderPath, 'both-screen', cb)
        }
    }
    //console.log('after that :',configObj)
}
function testIntegrity(configFolderPath, configObj, cb) {
    let integrityFlag = 1;
    configFolderPath = configFilePath.replace("settings.config", "")
    for (let i = 0; i < configObj.finalMerger.length; i++) {
        var obj = configObj.finalMerger[i]
        obj.cpath = obj.cpath.replace(/\\/g, '/')

        if (obj.type == 'camera') {
            obj.cpath = obj.cpath.replace(/\\/g, '/')
            setVideoDuration(configObj, 'camera', configFolderPath, i, cb)
            if (!fs.existsSync(obj.cpath)) {
                integrityFlag = 1;
                //break;
            }
        }
        else if (obj.type == 'screen') {
            obj.cpath = obj.cpath.replace(/\\/g, '/')
            setVideoDuration(configObj, 'screen', configFolderPath, i, cb)
            if (!fs.existsSync(obj.cpath)) {
                integrityFlag = 1;
                //break;
            }
        }
        else if (obj.type == 'both') {
            obj.cpath = obj.cpath.replace(/\\/g, '/')
            obj.spath = obj.spath.replace(/\\/g, '/')
            setVideoDuration(configObj, 'both', configFolderPath, i, cb)
            if (!fs.existsSync(obj.cpath)) {
                integrityFlag = 1;
                //break;
            }
            if (!fs.existsSync(obj.spath)) {
                integrityFlag = 1;
                //break;
            }
        }
    }
    // success -> 1
    // failure -> -1
    //console.log('returning obj',configObj)
    return integrityFlag
}
function loadConfig(cb) {
    dialog.showOpenDialog({
        browserWindow: electron.remote.getCurrentWindow(),
        title: 'Select File',
        buttonLabel: 'Select File',
        filters: [
            { name: 'Configuration', extensions: ['CONFIG'] }
        ],
        properties: ['openFile']
    }, function (filename) {
        if (filename != undefined) {
            configFilePath = filename[0]
            configObj = JSON.parse(fs.readFileSync(configFilePath, 'utf8'));

            status = testIntegrity(configFilePath, configObj, cb);
            //cb(status,configObj,configFilePath)
        }
        else {
            cb(404);
        }
    })
}

function addConfigFiles(configObj, configFilePath, cb) {
    for (let i = 0; i < configObj.finalMerger.length; i++) {
        let obj = configObj.finalMerger[i]
        // console.log(obj);
        //var trackLeft = ((obj.endTime - obj.startTime) * 10)
        var trackLeft = { start: obj.duration.startTime, end: obj.duration.endTime }
        //var trackLeft = obj.startTime * 10
        if (obj.type == 'camera' || obj.type == 'screen') {
            addConfigFiles_helper(configFilePath, obj.cpath, obj.type, obj.waveform, trackLeft, obj.windowCoords, cb)
        }
        else if (obj.type == 'both') {
            //addConfigFiles_helper(configFilePath,obj.spath,obj.type,cb)	
			/*
				In case of both path is an array
			*/
            addConfigFiles_helper(configFilePath, [obj.cpath, obj.spath], obj.type, obj.waveform, trackLeft, obj.windowCoords, cb)
        }
    }
}
function addConfigFiles_helper(configFilePath, path, type, img, trackLeft, windowCoords, cb) {
    if (type == 'both') {
        var filename = path[0].replace(/^.*[\\\/]/, '');
        this_row = a_file_row
        this_row = this_row.replace("filename_here", filename)
        this_row = this_row.replace("id_here", fileCounter)
        this_row = this_row.replace("file_type_here", type)
        $(".files").append(this_row)

        theFiles["file-" + fileCounter] = { filePath: configFilePath + path[0] }
        cb(path, "file-" + fileCounter, type, img, trackLeft, windowCoords)
        fileCounter++
    }
    else {
        var filename = path.replace(/^.*[\\\/]/, '');
        this_row = a_file_row
        this_row = this_row.replace("filename_here", filename)
        this_row = this_row.replace("id_here", fileCounter)
        $(".files").append(this_row)

        theFiles["file-" + fileCounter] = { filePath: configFilePath + path }
        cb(path, "file-" + fileCounter, type, img, trackLeft, windowCoords)
        fileCounter++
    }

}
module.exports = {
    addFile: addFile,
    removeFiles: removeFiles,
    testIntegrity: testIntegrity,
    loadConfig: loadConfig,
    addConfigFiles: addConfigFiles,
    onFileClick: onFileClick,
    addFileBackground: addFileBackground,
    getTheFiles: getTheFiles
};