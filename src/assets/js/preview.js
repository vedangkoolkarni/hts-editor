var child_process = require('child_process');
const { dialog } = require("electron").remote
var newText = "<div class='draggable resizeable text-overlay' id='txt-idhere' contenteditable='true'>Your Text Here...</div>"
var timer, timerFlag = true
var newCanvas = "<div class='draggable resizeable' style='display:none;'><canvas class='canvas' width='730px' height='390px'></canvas></div>";
var newImageCanvas = "<div class='draggable resizeable' style='display:inline-block'><div class='img-canvas'></div></div>";
var newVideo = "<video class='dummy-video' resized='false'><source src='' type='video/mp4'></video>"
var newTrack = "<div class='a-track even odd' id='track-id-here'><div class='track-name'>id-here</div> <div class='grid-stack'></div></div>"
var bothRecordingTypeCount = 1
var canvasCount = 1
var selectedCanvasId = undefined
var selectedCanvas = undefined
var sources = [];
var position = 0
var d = 0
var bottom_position = 1000
var below_position = 0
var above_position = 0
var ee;
var presetVideosFlag = false; // for compatibility with hifi recorder
var EventEmitter = require('event-emitter');
/*From videoUtils.js*/
var finalMerger = [{ "type": "both", "cpath": "./sample/camera/rec1.mkv", "spath": "./sample/screen/rec1.mkv" }]
let canvases = []
var title = 'previewwindow'
var nowPlayingVideo = []
var addtxtcnt = 0
var rulerJs = require("./ruler.js")
var muted;
var tracksJs = require('./tracks.js')





function startProcessing(areaObj, processType, cb) {
    let args = prepareCommand(processType, areaObj)
    //console.log('command : ',args)
    let flag = false
    let cmd = args.shift();
    let process = child_process.spawn(cmd, args);
    process.on('close', function () {
        cb({ "stop": true })
    });
    process.stdout.on('data', function (data) {
        //cb({data : data.toString()})
        //console.log(data.toString())
    })
    process.stderr.on('data', function (data) {
        console.log(data.toString())
        if (!flag) {
            cb({ title: title })

            flag = true
        }
        let d = data.toString()

        //console.log(d)
        //cb({data : d})
        // if(!d.includes('ffplay')){
        // 	cb({data : data.toString().split(".")[0]})	
        // }

    })
}
function prepareCommand(processType, areaSelectDivObj) {
    let cutPortionStr = getCutPortionString(areaSelectDivObj, 'video')
    //console.log('cut portion str = ',cutPortionStr)
	/*populateCanvasArr()
	canvases.sort(function(a, b){
	    return a.z_index - b.z_index
	})*/
    //let obj = canvases
    //console.log(obj)

    let obj
    var temp
    if (processType == 'preview') {
        obj = getCanvases('only-visible')
        temp = ['ffplay', '-i', obj[0].source, '-vf',];
    }
	/*else if(processType == 'process')
	{
		obj = getCanvases()
		temp =[ 'ffmpeg','-i',obj[0].source,'-vf',];
	}*/
    obj.sort(function (a, b) {
        return a.z_index - b.z_index
    })

    var lab = [];
    var len = obj.length;
    var str = "";

    var colorHex = $('#hex-color').val()
    var similarity = $('#similarity-range-slider').val() / 100
    var opacity = $('#opacity-range-slider').val() / 100
    //console.log(obj)
    for (var i = 1; i < len; i++) {
        var x = obj[i].source[0].replace((/\\/g), '/');
        var y = x.replace((/:/g), '\\\\:');
        str += "movie=" + y + "[i" + i + "];";
    }
    for (var i = 0; i < len; i++) {
        var templab = ""
        if (i == 0) {
            templab = '[in]'
        }
        else {
            templab = '[i' + i + ']'
        }
        lab.push(templab)
        var chromaKey = ""
        var cropCommand = ""
        if (obj[i].type == 'both-camera') {
            chromaKey = ',chromakey=0x' + colorHex + ':' + similarity + ':' + opacity
        }
        else if (obj[i].type == 'screen' || obj[i].type == 'both-screen') {
            var tempCoords = JSON.parse(obj[i].windowCoords)
            cropCommand = 'crop=' + parseInt(tempCoords.width - 20) + ':' + parseInt(tempCoords.height - 25) + ':' + parseInt(tempCoords.screenX + 10) + ':' + parseInt(tempCoords.screenY + 10) + ','
        }
        //console.log(cropCommand)
        str += templab + cropCommand + "scale=" + obj[i].width + ":" + obj[i].height + cutPortionStr + chromaKey + ",setsar=sar=1/1,setdar=dar=16/9" + templab + ";"
    }
    for (var i = 1; i < len; i++) {

        str += lab[i - 1] + lab[i] + "overlay=" + obj[i].left + ":" + obj[i].top;
        if (i != len - 1) {
            str += "[i" + i + "];";
        }
        else {
            str += "[out]";
        }
    }
    temp.push(str);
    for (let i in areaSelectDivObj) {
        temp.push('-af'); temp.push(getCutPortionString(areaSelectDivObj, 'audio'))
        break
    }
    if (processType == 'preview') {
        temp.push('-x'); temp.push('1')
        temp.push('-y'); temp.push('1')
        temp.push('-seek_interval'); temp.push('1')
        temp.push('-window_title'); temp.push(title)
        temp.push('-x'); temp.push('640')
        temp.push('-y'); temp.push('480')
        temp.push('-autoexit')
    }

    console.log('final command : ', temp);
    return temp
}
function getCutPortionString(areaSelectDivObj, type) {
    var str = '', counter = 0, portions = [0]
    for (let i in areaSelectDivObj) {
        portions.push(areaSelectDivObj[i].left / 10)
        portions.push(areaSelectDivObj[i].right / 10)
    }
    portions.push('t')

    if (portions.length > 2) {
        for (let i = 0; i < portions.length;) {
            if (str == '' && type == 'video') {
                str += ',select='
            }
            else if (str == '' && type == 'audio') {
                str += 'aselect='
            }
            str += "'between(t," + portions[i++] + "," + portions[i++] + ")'+"
        }
        str = str.substr(0, str.length - 1) //remove the last +
    }
    return str
}
function pausePreview(cb) {
    cb('completed')
}
function addText() {

    //console.log('new Text before replacing : ',newText)
    var appText = newText.replace("idhere", addtxtcnt)
    $("#background").append(appText)
    $(".resizeable").resizable({
        containment: "#background",
        handles: 'e,w,n,s,se,nw,ne,sw',

    });
    $(".draggable").draggable({
        cursor: "crosshair",
        containment: "#background",
        appendTo: "#background"
    });
    //console.log("count for id txt",addtxtcnt," newText : ",appText)
    addtxtcnt++
}
function canvasClick(ev) {
    //console.log('cliked on canvas')
	/*$("canvas").each(function(i,obj)
	{
		$(this).css('border','1px solid white')
	})
	$(ev.target).css('border','1px dashed black')*/
    selectedCanvasId = $(ev.target).attr('id')
    selectedCanvas = ev.target
    //console.log('clicked on canvas',selectedCanvas,selectedCanvasId)
}
function setCanvasId(recordingType, windowCoords) {
    let can = document.getElementById("background").lastElementChild.firstElementChild
    //console.log('windowCoords for this canvas: ',windowCoords)
    //console.log('recording type : in setCanvasId : ',recordingType)
    can.setAttribute('canvas_file_type', recordingType)
    if (recordingType.split('-')[0] == 'both') {
        $(can).attr('id', 'canvas-' + (canvasCount) + '-' + bothRecordingTypeCount)
        can.setAttribute('canvas-type', recordingType.split('-')[1])
        bothRecordingTypeCount++
        if (bothRecordingTypeCount == 3) {
            canvasCount++
            bothRecordingTypeCount = 1
        }
    }
    else {
        $(can).attr('id', 'canvas-' + (canvasCount))
        canvasCount++;
    }

    $(".canvas").each(function (index, obj) {
        if (position < $(obj).css('z-index')) {
            position = $(obj).css('z-index')
        }
    })
    position = position + 1
    $(can).css('z-index', position)
    $(can).parent().css('z-index', position)
    if (windowCoords != undefined)
        can.setAttribute('windowCoords', JSON.stringify(windowCoords))
}
function setVideoId(recordingType, windowCoords) {
    //console.log('windowCoords for this : ',windowCoords)
    var vid = document.getElementById("dummy-videos-wrap").lastElementChild
    //console.log(recordingType)
    if (recordingType.split('-')[0] == 'both') {
        $(vid).attr('id', 'video-' + (canvasCount) + '-' + bothRecordingTypeCount)
        vid.setAttribute('video-type', recordingType.split('-')[1])
    }
    else {
        $(vid).attr('id', 'video-' + (canvasCount))
    }
    if (windowCoords != undefined)
        vid.setAttribute('windowCoords', JSON.stringify(windowCoords))
}
function drawOnCanvas(type, param) {
    //console.log('param : ',param)
    let canvas = document.getElementById("background").lastElementChild.firstElementChild
    let ctx = canvas.getContext("2d");
    if (type == 'image') {
        let imgSource = param[0]
        //console.log(imgSource.replace(/\\/g,"/"))
        imgObj = new Image()
        imgObj.onload = function () {
            ctx.drawImage(imgObj, 0, 0, canvas.width, canvas.height)
        }
        imgObj.src = imgSource
    }
    else if (type == 'video') {
        let videoToLoad = $("#dummy-videos-wrap").children(":last")[0]
        let videoSource = param.path[0].replace(/\\/g, "/");
        //debugger
        /*videoSource = "file:///"+videoSource;*/
        $(videoToLoad).children("source").first().attr('src', videoSource);
        videoToLoad.load()
        var playPromise = videoToLoad.play()
        if (playPromise !== undefined) {
            playPromise.then(function () {
                // Automatic playback started!
                (function loop() {
                    if (!videoToLoad.paused && !videoToLoad.ended) {
                        drawWithSize(videoToLoad, ctx, canvas)
                        setTimeout(loop, 1000 / 30); // drawing at 30fps
                    }
                })();
                setTimeout(function () {
                    videoToLoad.currentTime = 0
                    videoToLoad.pause()
                }, 1000);
            }).catch(function (error) {
                console.log(error)
            });
        }
        //console.log('playing now', this.readyState)
        videoToLoad.addEventListener('play', function () {

            if (this.readyState > 0) {
                var $this = this; //cache
                (function loop() {
                    if (!$this.paused && !$this.ended) {
                        drawWithSize($this, ctx, canvas)
                        setTimeout(loop, 1000 / 30); // drawing at 30fps
                    }
                })();
            }
        })
        videoToLoad.addEventListener('ended', () => {
            var videos = $('video');
            var ended = 0; var paused = 0; var played = 0; var total = videos.length;
            for (let index = 0; index < videos.length; index++) {
                const element = videos[index];
                if (element.ended) {
                    ended++;
                }
                else if (element.paused) {
                    paused++;
                }
            }
            if (paused == 1 && (ended == total - 1)) {
                $("#playim").attr("src", "img/play.png");
                $("#play-video").attr("title", "Play");
            }
            // console.log('total : ', total, ' paused : ', paused, ' ended : ', ended);
        });
    }
}
function drawWithSize(elem, ctx, canvas) {
    /* Used to crop screen area*/
    var windowCoords = null
    if ((elem.getAttribute('windowCoords') != null)) {
        windowCoords = JSON.parse(elem.getAttribute('windowCoords'))
    }
    var background = {
        width: parseInt($('#background').css('width').split('px')[0]),
        height: parseInt($('#background').css('height').split('px')[0]),
        left: (parseInt($('#tab').css('width').split('px')[0]) + parseInt($('#file-lists').css('width').split('px')[0]))
    }
    var videoType = elem.getAttribute('video-type')
    var id = elem.id.replace('video', 'canvas')

    if (videoType == 'camera') {
        if (elem.getAttribute('resized') == 'false') {
            var par = $('#' + id)
            $(par).parent().css('width', (background.width * 0.4) + 'px')
            $(par).parent().css('height', (background.height * 0.4) + 'px')
            $(par).parent().css('left', (background.width * 0.6) + 'px')
            $(par).parent().css('top', (background.height * 0.6) + 'px')
            elem.setAttribute('resized', true)
        }
    }
    else if (videoType == 'screen' || videoType == null) // its a screen of type both 
    {
        if (elem.getAttribute('resized') == 'false') {
            $('#' + id).parent().css('width', background.width)
            elem.setAttribute('resized', true)
        }
    }
    if (windowCoords != null) {
        ctx.drawImage(elem, windowCoords.screenX + 10, windowCoords.screenY + 10, windowCoords.width - 20, windowCoords.height - 25, 0, 0, canvas.width, canvas.height);
    }
    else {
        ctx.drawImage(elem, 0, 0, canvas.width, canvas.height);
    }

}
function addCanvas(type, param, recordingType) {
    if (type == 'image') {
        sources.push(param[0])
        /*$("#background").append(newImageCanvas)*/
    }
    else if (type == 'video') {
        sources.push(param.path)
        /**/
        $("#dummy-videos-wrap").append(newVideo)
        setVideoId(recordingType, param.windowCoords)
    }

    $("#background").append(newCanvas)
    setCanvasId(recordingType, param.windowCoords)
    $(".resizeable").resizable({
        containment: "#background",
        handles: 'e,w,n,s,se,nw,ne,sw',
    });
    $(".draggable").draggable({
        cursor: "crosshair",
        containment: "#background",
    });

    drawOnCanvas(type, param)
}
function addImage(imgPath, recordingType, cb) {
    // let imgPath = dialog.showOpenDialog({properties : ['openFile'] , filters : [
    // { name : 'Images' , extensions : ['jpg','png']}
    // ]})

    if (imgPath != undefined) {
        addCanvas('image', imgPath, recordingType)
    }
}
function addVideo(vidObj, recordingType, cb) {
    // let vidPath = dialog.showOpenDialog({properties : ['openFile'] , filters : [
    // { name : 'Videos' , extensions : ['mkv','mp4','wav','mov']}
    // ]})

    //vidPath = vidPath[0]
    if (vidObj != undefined) {
        addCanvas('video', vidObj, recordingType)
    }
}

function removeCanvas(deletedIds, source) {
    // console.log(deletedIds)
    // console.log('source to delete : ',source)
    // console.log()
    for (let i in sources) {
        let tmp = sources[i].split('\\')[sources[i].split('\\').length - 1]
        if (tmp == source) {
            sources.splice((i), 1)
            $('#' + deletedIds[0].replace('file', 'canvas')).parent().remove()
            break
        }
        // console.log(tmp)
    }
    // console.log(selectedCanvasId)
    // let popper = parseInt(selectedCanvasId.split("-")[1])
    // $(selectedCanvas).parent().remove()
    // sources.splice((popper),1)
}

// //$("#bring-on-top").on('click',function(){
function bringOnTop() {
    //ee.emit("play")
    //console.log(canvasCount)
    position = canvasCount + 1
    //console.log(position)
    $(".canvas").each(function (index, obj) {
        if (position < $(obj).css('z-index')) {
            position = $(obj).css('z-index')
        }
    })
    //console.log(position)
    position = position + 1
    $(selectedCanvas).css('z-index', position)
}


function bringToBottom() {
    //console.log(canvasCount)
    //console.log( bottom_position)	
    $(".canvas").each(function (index, obj) {
        d = $(obj).css('z-index')
        //console.log(d)
        d = d + 1
        $(this).css('z-index', d)
        d = $(obj).css('z-index')
        //console.log(d)
    })
    $(".canvas").each(function (index, obj) {
        if (bottom_position > $(obj).css('z-index')) {
            bottom_position = $(obj).css('z-index')
        }
    })
    //console.log( bottom_position)
    bottom_position = bottom_position - 1
    $(selectedCanvas).css('z-index', bottom_position)
}

function below() {
    //console.log(canvasCount)	
    //console.log( below_position)
    d = $(selectedCanvas).css('z-index')
    //console.log(d)
    $(".canvas").each(function (index, obj) {
        if ($(obj).css('z-index') < d) {
            below_position = $(obj).css('z-index')
            $(obj).css('z-index', d)
            return
        }
    })
    $(selectedCanvas).css('z-index', below_position)
}


function above() {
    //console.log(canvasCount)

    //console.log( above_position)
    a = $(selectedCanvas).css('z-index')
    //console.log(d)
    $(".canvas").each(function (index, obj) {
        if ($(obj).css('z-index') > a) {
            above_position = $(obj).css('z-index')
            $(obj).css('z-index', a)
            return
        }
    })
    $(selectedCanvas).css('z-index', above_position)
}
function populateCanvasArr(mode) {
    total = $('.canvas').length, temp = 0
    //console.log(total)
    let background = {
        top: $("#background").prop("offsetTop"),
        left: $("#background").prop("offsetLeft"),
        width: $("#background").prop("offsetWidth"),
        height: $("#background").prop("offsetHeight"),
    }
    canvases = []
    $(".canvas").each(function (index, obj) {
        let width = $(obj).parent().css('width')
        let height = $(obj).parent().css('height')
        var leftT = $(obj).parent().css('left')
        var left = ((parseInt((leftT).substr(0, leftT.length - 2)) - background.left))
        var topT = $(obj).parent().css('top')
        var top = ((parseInt((topT).substr(0, topT.length - 2)) - background.top))

        var padding = getVideoPadding(obj)
        //console.log(padding,this)
        var objToPush = {
            width: (parseInt((width).substr(0, width.length - 2))),
            height: (parseInt((height).substr(0, height.length - 2))),
            left: left,
            top: top,
            widthper: Math.round((Math.round((width).substr(0, width.length - 2)) / background.width) * 100),
            heightper: Math.round((Math.round((height).substr(0, height.length - 2)) / background.height) * 100),
            leftper: Math.round((left / background.width) * 100),
            topper: Math.round((top / background.height) * 100),
            source: sources[index],
            z_index: parseInt($(this).css("z-index")),
            id: obj.id,
            type: obj.getAttribute('canvas_file_type'),
            windowCoords: obj.getAttribute('windowCoords'),
            padding: padding,
            trasnparency: {}
        }

        console.log(objToPush);
        if (mode == 'only-visible') {
            if ($(obj).parent().css('display') != 'none') {
                canvases.push(objToPush)
            }
        }
        else {
            canvases.push(objToPush)
        }
        temp++
    })

}
function getVideoPadding(obj) {

    let background = {
        top: $("#background").prop("offsetTop"),
        left: $("#background").prop("offsetLeft"),
        width: $("#background").prop("offsetWidth"),
        height: $("#background").prop("offsetHeight"),
    }
    //widthper : Math.ceil((Math.ceil((width).substr(0,width.length - 2)) / background.width)*100),
    var borderLeft = parseInt($(obj).attr('borderLeft'))
    var borderTop = parseInt($(obj).attr('borderTop'))
    var borderRight = parseInt($(obj).attr('borderRight'))
    var borderBottom = parseInt($(obj).attr('borderBottom'))

    console.log(borderLeft, borderTop, borderRight, borderBottom)
    // var paddingW  = (borderRight+borderRight) ?  (borderRight+borderRight) : 0
    // var paddingH  = (borderTop+borderBottom) ? (borderTop+borderBottom) : 0
    var paddingTop_x = borderTop ? borderTop : 0
    var paddingBottom_x = borderBottom ? borderBottom : 0
    var paddingLeft_x = borderLeft ? borderLeft : 0
    var paddingRight_x = borderRight ? borderRight : 0
    var paddingColor_x = $(obj).attr('borderColor')

    if (paddingColor_x == undefined) {
        paddingColor_x = "ffffff"
    }
    return {
        paddingTop: parseFloat((paddingTop_x / background.height) * 100) * 7.2,
        paddingBottom: parseFloat((paddingBottom_x / background.height) * 100) * 7.2,
        paddingLeft: parseFloat((paddingLeft_x / background.width) * 100) * 12.8,
        paddingRight: parseFloat((paddingRight_x / background.width) * 100) * 12.8,
        paddingW: (parseFloat((paddingLeft_x / background.width) * 100) * 12.8) + (parseFloat((paddingRight_x / background.width) * 100) * 12.8),
        paddingH: (parseFloat((paddingTop_x / background.height) * 100) * 7.2) + (parseFloat((paddingBottom_x / background.height) * 100) * 7.2),
        paddingColor: paddingColor_x
    }
}
function rgbToHex(color) {
    //console.log(color)
}
function getCanvases(mode) {
    populateCanvasArr(mode)
    return canvases
}
function playFromCanvas(allFileLengths, areaSelectDivObj) {

    var currentPinPos = $('.current-pin').css('left').split('px')[0]
    var nowPlayingId = null, type = null
    nowPlayingId = getNowPlayingId(allFileLengths)
    //console.log('playingFromCanvas : ', nowPlayingId)

    if (nowPlayingId != null) {
        showNowPlayingCanvas(nowPlayingId, allFileLengths)
        if (allFileLengths[nowPlayingId].type == 'both') {
            var videoToPlay = $('#' + nowPlayingId.replace('track', 'video') + '-1').get(0)
            var videoToPlay2 = $('#' + nowPlayingId.replace('track', 'video') + '-2').get(0)

            setVideoTimeByPinPos([videoToPlay, videoToPlay2], allFileLengths)

            videoToPlay.play(); videoToPlay2.play()
            videoToPlay2.addEventListener('timeupdate', function () {
                canvasVideoTimeUpdate(this, this.currentTime, nowPlayingId, allFileLengths)
            })
            videoToPlay2.addEventListener('ended', function () {
                canvasVideoEnded(allFileLengths)
            })
            nowPlayingVideo = []
            nowPlayingVideo[0] = $('#' + nowPlayingId.replace('track', 'video') + '-1').get(0)
            nowPlayingVideo[1] = $('#' + nowPlayingId.replace('track', 'video') + '-2').get(0)
        }
        else {
            var videoToPlay = $('#' + nowPlayingId.replace('track', 'video')).get(0)
            //console.log('currentPinPos : ',currentPinPos)
            //console.log('currentTime 1 : ',videoToPlay.currentTime)

            setVideoTimeByPinPos([videoToPlay], allFileLengths)

            videoToPlay.play()
            videoToPlay.addEventListener('timeupdate', function () {
                canvasVideoTimeUpdate(this, this.currentTime, nowPlayingId, allFileLengths)
            })
            videoToPlay.addEventListener('ended', function () {
                canvasVideoEnded(allFileLengths)
            })
            nowPlayingVideo = []
            nowPlayingVideo[0] = videoToPlay
        }
    }
}
function setVideoTimeByPinPos(video, allFileLengths) {
    var trackContentScrollLeft = $('.track-content-wrap').scrollLeft();
    var currentPinPos = parseInt($('.current-pin').css('left').split('px')[0]) + trackContentScrollLeft;

    // var currentPinPosSeconds = parseInt($('.current-pin').css('left').split('px')[0]) / 10
    var currentPinPosSeconds = currentPinPos / 10;
    var nowPlayingId = getNowPlayingId(allFileLengths);
    showNowPlayingCanvas(nowPlayingId, allFileLengths);
    var leftSeconds = allFileLengths[nowPlayingId].left / 10;

    if (video.length == 2) //both mode
    {
        video[0].currentTime = currentPinPosSeconds - leftSeconds;
        video[1].currentTime = currentPinPosSeconds - leftSeconds;
    }
    else if (video.length == 1) {
        video[0].currentTime = currentPinPosSeconds - leftSeconds;
    }
}
function getNowPlayingId(allFileLengths) {

    var trackContentScrollLeft = $('.track-content-wrap').scrollLeft();
    var currentPinPos = parseInt($('.current-pin').css('left').split('px')[0]) + trackContentScrollLeft;
    var nowPlayingId = null;

    //console.log('allFileLengths in getNowPlayingId : ',allFileLengths)
    for (var key in allFileLengths) {
        if (currentPinPos >= allFileLengths[key].left && currentPinPos <= allFileLengths[key].right) {
            nowPlayingId = key;
            break;
        }
    }
    return nowPlayingId
}
function showNowPlayingCanvas(nowPlayingId, allFileLengths) {
    hideOtherCanvases()
    //console.log(nowPlayingId,allFileLengths)
    if (allFileLengths[nowPlayingId].type == 'both') {
        var canvasToPlay = $('#' + nowPlayingId.replace('track', 'canvas') + '-1').get(0)
        var canvasToPlay2 = $('#' + nowPlayingId.replace('track', 'canvas') + '-2').get(0)
        //console.log('canvas to show : ',canvasToPlay,canvasToPlay2)
        $(canvasToPlay).parent().show(); $(canvasToPlay2).parent().show()
    }
    else {
        var canvasToPlay = $('#' + nowPlayingId.replace('track', 'canvas')).get(0)
        //console.log('canvas to show : ',canvasToPlay,canvasToPlay2)
        $(canvasToPlay).parent().show()
    }
}
function hideOtherCanvases() {
    $('.canvas').each(function () {
        $(this).parent().hide()
    })
}
function pauseFromCanvas(allFileLengths) {

    console.log('pause from canvas was called');
    var nowPlayingId = getNowPlayingId(allFileLengths)
    if (nowPlayingId != null) {
        if (allFileLengths[nowPlayingId].type == 'both') {
            var videoToPlay = $('#' + nowPlayingId.replace('track', 'video') + '-1').get(0)
            var videoToPlay2 = $('#' + nowPlayingId.replace('track', 'video') + '-2').get(0)
            videoToPlay.pause(); videoToPlay2.pause()
        }
        else {
            var videoToPlay = $('#' + nowPlayingId.replace('track', 'video')).get(0)
            videoToPlay.pause()
        }
    }
}

function seekFromSeekBar(currentSeekPosition) {
    var allFileLengths = tracksJs.getAllFileLengths()
    if (Object.keys(allFileLengths).length > 0) {
        pauseFromCanvas(allFileLengths)
        $("#playim").attr("src", "img/play.png");
        $("#play-video").attr("title", "Play");
        var count = 0;
        for (var c in allFileLengths) {
            count = count + 1;
        }
        var val = Math.round((allFileLengths['track-' + count].right / 100) * currentSeekPosition);
        $('.current-pin').css('left', (val) + 'px')
        seekFromRuler(tracksJs.getAllFileLengths(), false)
    }
}



function seekFromRuler(allFileLengths, flag) {
    if (flag) {
        pauseAllVideos();
    }
    var currentPinPosSeconds = parseInt($('.current-pin').css('left').split('px')[0]) / 10
    var nowPlayingId = getNowPlayingId(allFileLengths)
    showNowPlayingCanvas(nowPlayingId, allFileLengths)
    var leftSeconds = allFileLengths[nowPlayingId].left / 10
    if (nowPlayingId != null) {
        if (allFileLengths[nowPlayingId].type == 'both') {
            var videoToPlay = $('#' + nowPlayingId.replace('track', 'video') + '-1').get(0)
            var videoToPlay2 = $('#' + nowPlayingId.replace('track', 'video') + '-2').get(0)
            videoToPlay.currentTime = currentPinPosSeconds - leftSeconds; videoToPlay2.currentTime = currentPinPosSeconds - leftSeconds

        }
        else {
            var videoToPlay = $('#' + nowPlayingId.replace('track', 'video')).get(0)
            videoToPlay.currentTime = currentPinPosSeconds - leftSeconds

        }

        console.log(currentPinPosSeconds)
        if (flag) {
            playFromCanvas(allFileLengths)
        }
    }
}
function MuteSection(allFileLengths) {

    var currentPinPosSeconds = parseInt($('.current-pin').css('left').split('px')[0]) / 10
    var nowPlayingId = getNowPlayingId(allFileLengths)
    showNowPlayingCanvas(nowPlayingId, allFileLengths)
    var leftSeconds = allFileLengths[nowPlayingId].left / 10
    if (nowPlayingId != null) {
        if (allFileLengths[nowPlayingId].type == 'both') {
            s
            var videoToPlay = $('#' + nowPlayingId.replace('track', 'video') + '-1').get(0)
            var videoToPlay2 = $('#' + nowPlayingId.replace('track', 'video') + '-2').get(0)
            videoToPlay.muted = true;
        }
        else {
            var videoToPlay = $('#' + nowPlayingId.replace('track', 'video')).get(0)
            videoToPlay.muted = true;
            console.log('new current time : ', videoToPlay.currentTime)
        }
        //playFromCanvas(allFileLengths)
    }
}
function pauseAllVideos() {
    $('video').each(function () {
        this.currentTime = 0
        this.pause()
    })
}


function canvasVideoTimeUpdate(elem, currentTime, nowPlayingId, allFileLengths) {
    var count = 0;
    for (var c in allFileLengths) {
        count = count + 1;
    }
    var areaSelectDivObj = rulerJs.getAreaSelectDivObj();
    var trackContentScrollLeft = $('.track-content-wrap').scrollLeft();
    var currentPinPos = parseInt($('.current-pin').css('left').split('px')[0]) + trackContentScrollLeft;
    // var currentPinPos = parseInt($('.current-pin').css('left').split('px'))

    var val = Math.round(((currentPinPos) / allFileLengths['track-' + count].right) * 100);

    $('#videoRange').val(val);
    $('#videoProgress').val(val);

    var xLeft = (allFileLengths[nowPlayingId].left + (currentTime * 10)) - trackContentScrollLeft;

    $('.current-pin').css('left', xLeft + 'px');
    for (let x in areaSelectDivObj) {
        if (currentPinPos >= areaSelectDivObj[x].left && currentPinPos < areaSelectDivObj[x].right) {
            $('.current-pin').css('left', (xLeft + areaSelectDivObj[x].width) + 'px')
            seekFromRuler(allFileLengths, true)
        }
    }

    var areaSelectMuteDivObj = rulerJs.getAreaSelectMuteDivObj()

    for (let x in areaSelectMuteDivObj) {

        if (currentPinPos >= areaSelectMuteDivObj[x].left && currentPinPos < areaSelectMuteDivObj[x].right) {
            MuteSection(allFileLengths, 'muted')
            muted = true;
            break;
        }
    }
    if (muted == false) {
        MuteSection(allFileLengths, 'unmute')
    }

}





function canvasVideoEnded(allFileLengths) {
    $('.current-pin').css('left', parseInt($('.current-pin').css('left').split('px')[0]) + 5)

    playFromCanvas(allFileLengths)
}
function getSelectedCanvas() {
    //return $(selectedCanvas).parent().get(0)
    return $(selectedCanvas).get(0)
}
module.exports = {
    startProcessing: startProcessing,
    prepareCommand: prepareCommand,
    pausePreview: pausePreview,
    addText: addText,
    addImage: addImage,
    addVideo: addVideo,
    bringOnTop: bringOnTop,
    bringToBottom: bringToBottom,
    below: below,
    above: above,
    populateCanvasArr: populateCanvasArr,
    removeCanvas: removeCanvas,
    getCanvases: getCanvases,
    playFromCanvas: playFromCanvas,
    pauseFromCanvas: pauseFromCanvas,
    seekFromRuler: seekFromRuler,
    pauseAllVideos: pauseAllVideos,
    canvasClick: canvasClick,
    getSelectedCanvas: getSelectedCanvas,
    seekFromSeekBar: seekFromSeekBar
};
