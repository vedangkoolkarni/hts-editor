const { exec } = require('child_process');
var child_process = require('child_process');
var startPinMoved = false
var endPinMoved = false
let pinPointingBottom = 'polygon(0% 0%, 100% 0%, 50% 100%, 50% 100%)'
let pinPointingUp = 'polygon(0% 100%, 40% 0%, 50% 0%, 100% 100%)'
let areaSelectDivInserted = false
let trackSelectDivInserted = false
let selectedTrackContentId = 0
let areaSelectId = 0
let areaSelectDiv = "<div class='current-area-select area-select' id='area-select-id-here'></div>"
let areaSelectDivObj = {}
let areaSelectMuteDivObj = {}
let conflictsWith = []
var muteDuration = []
let pinInterval
var Files = []
var FileCnt = 0
let j = 0
let startLeft
let endLeft
let trackId
let trackIdOfareaSelected
let totalwidth = 120
let h = 0, m = 0, s = 0
let a_sec = "<div class='a-sec'>|</div>"
let a_label = "<div class='a-sec-label'></div>"

$(document).ready(function () {

})
function setRulerWidth(width) {
    // totalwidth = (width / 10) - 440;
    totalwidth = width / 10;
    //console.log('need to update ruler', totalwidth);
    drawRuler();
}
function drawRuler() {
    for (let i = 1; i <= totalwidth; i++) {
        if (s == 60) {
            s = 0;
            m++
            if (m == 60) {
                m = 0;
                h++;
            }
        }
        $(".rule").append(a_sec)
        if (s % 10 == 0) {
            $(".labels").append(a_label);
            $(".a-sec-label:last").text(h + ':' + m + ':' + s);
            $(".a-sec:last").css('color', 'crimson');
        }
        else {
            $(".labels").append(a_label);
        }
        s++;
    }
    // ! 100px because of left of ruler tag is 100px;
    // $('.tracks').css('width', 'calc(' + $('.rule').css('width') + ' + 100px)')
    // $('.timer-pins-wrap').css('width', $('.rule').css('width'))
    $('.track-content-inner-wrap').css('width', 'calc(' + $('.rule').css('width') + ' + 100px)')
}
function handlePinMotion() {
    $(".current-pin").draggable({
        axis: 'x',
        containment: '.ruler'
    })
    $(".start-pin").draggable({
        axis: 'x',
        containment: '.ruler'
    })
    $(".end-pin").draggable({
        axis: 'x',
        containment: '.ruler'
    })
}
$(document).on('drag', '.current-pin', function (ev, ui) {
    let currentLeft = $(".current-pin").css('left')
    console.log('current pin left : ', currentLeft)
    if (!startPinMoved)
        $(".start-pin").css('left', currentLeft)
    if (!endPinMoved)
        $(".end-pin").css('left', currentLeft)
    if (areaSelectDivInserted) {
        doDrawAreaSelectDiv()
    }
    //needs a check @note
    if (trackSelectDivInserted) {
        doDrawAreaSelectDiv()
    }
})
$(document).on("dragstop", ".current-pin", function (ev, ui) {
    let startLeft = parseInt($(".start-pin").css('left').split("px")[0])
    let currentLeft = parseInt($(".current-pin").css('left').split("px")[0])
    let endLeft = parseInt($(".end-pin").css('left').split("px")[0])

    if (currentLeft == startLeft) {
        let x = $(".start-pin-head").get(0)
        x.style.clipPath = 'polygon(0% 100%, 40% 0%, 50% 0%, 100% 100%)'
        x.style.left = '-16px'
        startPinMoved = false
    }
    if (currentLeft == endLeft) {
        let x = $(".end-pin-head").get(0)
        x.style.clipPath = 'polygon(0% 100%, 60% 0%, 100% 100%, 0% 100%)'
        x.style.left = '0px'
        endPinMoved = false
    }
    if (startLeft > endLeft) {
        swapPins()
    }
})
$(document).on("dragstart", ".start-pin", function () {
    startPinMoved = true
    let x = $(".start-pin-head").get(0)
    x.style.clipPath = 'polygon(0% 0%, 100% 0%, 50% 100%, 50% 100%)'
    x.style.left = '-8px'
    if (!areaSelectDivInserted) {
        insertAreaSelectDiv()
    }
    if (!trackSelectDivInserted) {
        insertAreaSelectTrack()
    }
})
$(document).on('drag', '.start-pin', function (ev, ui) {
    if (areaSelectDivInserted) {
        doDrawAreaSelectDiv()
    }
    if (trackSelectDivInserted) {
        doDrawAreaSelectDiv()
    }
})
$(document).on("dragstop", ".start-pin", function () {

    let startLeft = parseInt($(".start-pin").css('left').split("px")[0])
    let currentLeft = parseInt($(".current-pin").css('left').split("px")[0])
    let endLeft = parseInt($(".end-pin").css('left').split("px")[0])

    if (startLeft == currentLeft) {
        let x = $(".start-pin-head").get(0)
        x.style.clipPath = 'polygon(0% 100%, 40% 0%, 50% 0%, 100% 100%)'
        x.style.left = '-16px'
        startPinMoved = false
    }
    if (startLeft > endLeft) {
        swapPins()
    }
    if (areaSelectDivInserted) {
        doDrawAreaSelectDiv()
    }
    if (trackSelectDivInserted) {
        doDrawAreaSelectDiv()
    }
})
$(document).on("dragstart", ".end-pin", function () {
    endPinMoved = true
    let x = $(".end-pin-head").get(0)
    x.style.clipPath = 'polygon(0% 0%, 100% 0%, 50% 100%, 50% 100%)'
    x.style.left = '-8px'
    if (!areaSelectDivInserted) {
        //insertAreaSelectDiv()
    }
    if (!trackSelectDivInserted) {
        insertAreaSelectTrack()
    }
})
$(document).on('drag', '.end-pin', function (ev, ui) {
    if (areaSelectDivInserted) {
        doDrawAreaSelectDiv()
    }
    if (trackSelectDivInserted) {
        doDrawAreaSelectDiv()
    }
})
$(document).on("dragstop", ".end-pin", function () {

    let startLeft = parseInt($(".start-pin").css('left').split("px")[0])
    let currentLeft = parseInt($(".current-pin").css('left').split("px")[0])
    let endLeft = parseInt($(".end-pin").css('left').split("px")[0])
    if (endLeft == currentLeft) {
        let x = $(".end-pin-head").get(0)
        x.style.clipPath = 'polygon(0% 100%, 60% 0%, 100% 100%, 0% 100%)'
        x.style.left = '0px'
        endPinMoved = false
    }
    if (startLeft > endLeft) {
        swapPins()
    }
    if (areaSelectDivInserted) {
        doDrawAreaSelectDiv()
    }
    if (trackSelectDivInserted) {
        doDrawAreaSelectDiv()
    }
})
$(document).on('dragstop', '.cut-area-select', function () {
    //console.log('dragstop cut area')
    if (checkAreaSelectDivInsertion('on-drag-resize', this)) {
        doDrawAreaSelectDiv()
    }
})
$(document).on('resizestop', '.cut-area-select', function () {
    //console.log('resizestop cut area')
    if (checkAreaSelectDivInsertion('on-drag-resize', this)) {
        doDrawAreaSelectDiv()
    }
})
$(document).on('dragstop', '.cut-area-of-track-select', function () {
    //console.log('dragstop cut area')
    if (checkMutedAreaSelectDivInsertion('on-drag-resize', this)) {
        doDrawAreaSelectDiv()
    }
})
$(document).on('resizestop', '.cut-area-of-track-select', function () {
    console.log('resizestop cut area of track select')
    if (checkMutedAreaSelectDivInsertion('on-drag-resize', this)) {
        doDrawAreaSelectDiv()
    }
})

function swapPins() {
    let startPinHead = $(".start-pin-head")
    let endPinHead = $(".end-pin-head")
    let startPin = $(".start-pin")
    let endPin = $(".end-pin")

    $(startPinHead).removeClass("start-pin-head")
    $(endPinHead).removeClass("end-pin-head")
    $(startPin).removeClass("start-pin")
    $(endPin).removeClass("end-pin")

    $(startPinHead).addClass("end-pin-head")
    $(endPinHead).addClass("start-pin-head")
    $(startPin).addClass("end-pin")
    $(endPin).addClass("start-pin")

    let temp = startPinMoved
    startPinMoved = endPinMoved
    endPinMoved = temp

    if (startPinMoved && endPinMoved) {
        let x = $(".start-pin-head").get(0)
        x.style.clipPath = pinPointingBottom
        x.style.left = '-8px'

        let y = $(".end-pin-head").get(0)
        y.style.clipPath = pinPointingBottom
        y.style.left = '-8px'
    }
    else if (!startPinMoved && endPinMoved) {
        let x = $(".start-pin-head").get(0)
        x.style.clipPath = pinPointingUp
        x.style.left = '-16px'

        let y = $(".end-pin-head").get(0)
        y.style.clipPath = pinPointingBottom
        y.style.left = '-8px'
    }
    else if (startPinMoved && !endPinMoved) {
        let x = $(".start-pin-head").get(0)
        x.style.clipPath = pinPointingBottom
        x.style.left = '-8px'

        let y = $(".end-pin-head").get(0)
        y.style.clipPath = pinPointingUp
        y.style.left = '2px'
    }
}

function animatePin(state) {
    if (state == 'start') {
		/*let left = parseInt($(".current-pin").css('left').split("px")[0]);
		pinInterval = setInterval(function(){
			left++;
			$(".current-pin").css('left',left)
		},100)*/
        var start;
        var nextAt;

        var f = function () {
            let left = parseInt($(".current-pin").css('left').split("px")[0]);
            left++;
            $(".current-pin").css('left', left)
            if (!start) {
                start = new Date().getTime();
                nextAt = start;
            }
            nextAt += 100;

            var drift = (new Date().getTime() - start) % 100;
            //$('<li>').text(drift + "ms").appendTo('#results');
            //console.log(drift + "ms")

            setTimeout(f, nextAt - new Date().getTime());
        };

        f();
    }
}
function insertAreaSelectDiv() {

    let x = $('.current-area-select').get(0)
    if (x == undefined) {
        // $('.timer-pins-wrap').append(areaSelectDiv.replace('id-here', areaSelectId++))
        $('.track-content-wrap').append(areaSelectDiv.replace('id-here', areaSelectId++))
    }
    areaSelectDivInserted = true
}
//code by Girish
function mutePortion() {
    if (checkMutedAreaSelectDivInsertion('on-mute')) {
        doDrawAreaSelectDiv()
    }
    var Id = selectedTrackContentId
    trackSelectDivInserted = false
    var id = Id.split('-')[3];
    var left_of_track = parseInt($("#" + selectedTrackContentId).css('left').split("px")[0])
    console.log(left_of_track, typeof left_of_track)
    id = id - 1
    trackID = id
    let x1 = $('.current-area-select').get(0)
    let ss = (parseInt($('.start-pin').css('left').split("px")[0]) - left_of_track) / 10
    let et = (parseInt($('.end-pin').css('left').split("px")[0]) - left_of_track) / 10
    let volume = parseInt($('.vProgress' + (id + 1)).attr('value'))
    if (muteDuration[id] == undefined) {
        muteDuration[id] = []
    }
    if (muteDuration[id] != undefined) {
        muteDuration[id].push({ 'ss': ss, 'et': et, 'volume': volume, 'AreaId': x1.id });
    }
    $('.start-pin').css('left', '0px')
    $('.end-pin').css('left', '0px')
    $(x1).removeClass('current-area-select')
    $(x1).addClass('cut-area-of-track-select')
    $(x1).resizable({
        axis: 'x',
        containment: '#' + selectedTrackContentId,
        handles: 'e,w',
        resize: function (ev, ui) {
            for (var i = 0; i < muteDuration[id].length; i++) {
                if (muteDuration[id][i] != undefined) {
                    if (muteDuration[id][i].AreaId == ev.target.id) {
                        let new_ss = (parseInt($('#' + ev.target.id).css('left').split("px")[0])) / 10
                        let new_width = parseInt($('#' + ev.target.id).css('width').split("px")[0])
                        let new_et = parseInt(new_ss + (new_width / 10))
                        muteDuration[id][i].ss = new_ss
                        muteDuration[id][i].et = new_et
                    }
                }
            }
            updateTrackAreaSelectDivObj(this, this.id)
        }
    })
    $(x1).draggable({
        axis: 'x',
        containment: '#' + selectedTrackContentId,
        drag: function (ev) {
            for (var i = 0; i < muteDuration[id].length; i++) {
                if (muteDuration[id][i] != undefined) {
                    if (muteDuration[id][i].AreaId == ev.target.id) {
                        let new_ss = (parseInt($('#' + ev.target.id).css('left').split("px")[0])) / 10
                        let new_width = parseInt($('#' + ev.target.id).css('width').split("px")[0])
                        let new_et = parseInt(new_ss + (new_width / 10))
                        muteDuration[id][i].ss = new_ss
                        muteDuration[id][i].et = new_et
                    }
                }

            }
            updateTrackAreaSelectDivObj(this, this.id)
        }
    })
    areaSelectMuteDivObj[x1.id] =
        {
            left: parseInt(x1.style.left.split('px')[0]),
            width: parseInt(x1.style.width.split('px')[0]),
            right: parseInt(x1.style.left.split("px")[0]) + parseInt(x1.style.width.split("px")[0]),
            trackID: trackID
        }
    console.log(areaSelectMuteDivObj)
}
function process(file) {
    var cropArgs = [];
    var volumeCmd = [];
    amixCommand = ''
    amixFiles = ''
    volumeCommand = ''
    console.log(file)
    for (let key in file) {
        Files.push(file[key]['filePath'])
        FileCnt++
    }
    console.log(Files)
    console.log(FileCnt)
    for (i = 0; i < FileCnt; i++) {
        console.log(muteDuration)


        if (muteDuration[i] != undefined) {
            volumeCmd[i] = '[' + i + ']'
            amixFiles += '[temp' + i + ']'
            console.log(muteDuration)
            for (j = 0; j < muteDuration[i].length; j++) {
                console.log(muteDuration[i][j])
                if (muteDuration[i][j] != undefined) {
                    if (j == 0) {
                        volumeCmd[i] += 'volume=enable=\'between(t,' + muteDuration[i][j].ss + ',' + muteDuration[i][j].et + ')\':volume=' + muteDuration[i][j].volume
                    }
                    else {
                        volumeCmd[i] += ',volume=enable=\'between(t,' + muteDuration[i][j].ss + ',' + muteDuration[i][j].et + ')\':volume=' + muteDuration[i][j].volume
                    }
                }

            }
        }
        else {
            amixFiles += '[' + i + ']'
        }

    }
    for (i = 0; i < volumeCmd.length; i++) {
        console.log(volumeCmd[i], typeof volumeCmd[i], volumeCmd)
        if (volumeCmd[i] != undefined) {
            volumeCommand += volumeCmd[i]
            volumeCommand += '[temp' + i + '];'
            console.log("innnn")
        }
        else {
            console.log("outr")
        }

    }

    console.log(volumeCommand);
	/*for(i=0;i<FileCnt;i++)
	{
		amixFiles+='[temp'+i+']'
	}*/
    amixCommand = amixFiles + 'amix=inputs=' + FileCnt + ':duration=first:dropout_transition=3'
    console.log(amixCommand)
    fileCommand = []
    for (i = 0; i < FileCnt; i++) {
        fileCommand.push('-i')
        fileCommand.push('' + Files[i])
    }
    console.log(fileCommand)
    volumeCommand += amixCommand
    console.log(volumeCommand)
    //console.log(amixCommand)

    cropArgs = [
        'ffmpeg',
        '-filter_complex', volumeCommand,
        '-y', 'output1.mp4'
        //volume=enable=\'between(t,5,35)\':volume=0
    ];
    cropArgs.splice.apply(cropArgs, [1, 0].concat(fileCommand));
    console.log(cropArgs);

    var cmdC = cropArgs.shift();
    var procO = child_process.spawn(cmdC, cropArgs);

    procO.stderr.on('data', function (data) {
        console.log("sucess", data.toString());
    });

}
function getTrackContentID(selectedTrackContentId1) {
    selectedTrackContentId = selectedTrackContentId1
    console.log(selectedTrackContentId)
}
function insertAreaSelectTrack() {

    let x = $('.current-area-select').get(0)
    console.log(selectedTrackContentId);
    if (x == undefined) {
        $('#' + selectedTrackContentId).append(areaSelectDiv.replace('id-here', areaSelectId++))
    }
    trackSelectDivInserted = true
}
//
function doDrawAreaSelectDiv() {
    if (selectedTrackContentId != 0) {
        var left_of_track = $("#" + selectedTrackContentId).css('left').split("px")[0]
        startLeft = parseInt($('.start-pin').css('left').split("px")[0]) - left_of_track
        endLeft = parseInt($('.end-pin').css('left').split("px")[0]) - left_of_track
    }
    if (selectedTrackContentId == 0) {
        startLeft = parseInt($('.start-pin').css('left').split("px")[0])
        endLeft = parseInt($('.end-pin').css('left').split("px")[0])
    }
    console.log(startLeft, endLeft);

    $('.current-area-select').css('left', startLeft + 'px')
    $('.current-area-select').css('width', (endLeft - startLeft) + 'px')
}
//$(document).on('click','#cut',function(ev)
function cutPortion(cb) {

    console.log()
    var currentPortion = $('.current-area-select').get(0)
    if (currentPortion != undefined) {

        if (checkAreaSelectDivInsertion('on-cut')) {
            doDrawAreaSelectDiv()
        }
        areaSelectDivInserted = false
        $('.start-pin').css('left', '0px')
        $('.end-pin').css('left', '0px')
        let x1 = $('.current-area-select').get(0)
        $(x1).removeClass('current-area-select')
        $(x1).addClass('cut-area-select')
        $(x1).resizable({
            axis: 'x',
            containment: '.timer-pins-wrap',
            handles: 'e,w',
            resize: function (ev, ui) {
                updateAreaSelectDivObj(this, this.id)
            }
        })
        $(x1).draggable({
            axis: 'x',
            containment: '.timer-pins-wrap',
            drag: function () {
                updateAreaSelectDivObj(this, this.id)
            }
        })

        areaSelectDivObj[x1.id] =
            {
                left: parseInt(x1.style.left.split('px')[0]),
                width: parseInt(x1.style.width.split('px')[0]),
                right: parseInt(x1.style.left.split("px")[0]) + parseInt(x1.style.width.split("px")[0])
            }

        let x = $(".start-pin-head").get(0)
        x.style.clipPath = pinPointingBottom
        x.style.left = '-8px'

        let y = $(".end-pin-head").get(0)
        y.style.clipPath = pinPointingBottom
        y.style.left = '-8px'
    }
    //console.log(areaSelectDivObj)
}
$(document).on('click', '.area-select', function (ev) {
	/*let endLeft = (parseInt($(this).css('left').split("px")[0]) + parseInt($(this).css('width').split("px")[0]))+'px'
	$('.start-pin').css('left',$(this).css('left'))
	$('.end-pin').css('left',endLeft)
	$(this).addClass('current-area-select')*/
})
function checkAreaSelectDivInsertion(type, el) {
    let areaLeft, areaRight
    if (type == 'on-cut') {
        areaLeft = parseInt($('.start-pin').css('left').split("px")[0])
        areaRight = parseInt($('.end-pin').css('left').split("px")[0])
    }
    else if (type == 'on-drag-resize') {
        areaLeft = parseInt($(el).css('left').split("px")[0])
        areaRight = parseInt($(el).css('left').split("px")[0]) + parseInt($(el).css('width').split("px")[0])
        //console.log('id of this : ',el.id)
    }

    let min = 999999999, max = -1

    let conflictFlag = false
    for (let x in areaSelectDivObj) {
        if (type == 'on-drag-resize' && el.id == x) {
            continue
        }
        conflictFlag = false
        if (areaLeft >= areaSelectDivObj[x].left && areaLeft <= areaSelectDivObj[x].right && areaRight <= areaSelectDivObj[x].right && areaRight >= areaSelectDivObj[x].left) {
            //console.log('left in right in with '+x)
            conflictFlag = true
        }
        if (areaRight >= areaSelectDivObj[x].right && areaLeft >= areaSelectDivObj[x].left && areaLeft <= areaSelectDivObj[x].right) {
            //console.log('left in and right out with '+x)
            conflictFlag = true
        }
        if (areaLeft <= areaSelectDivObj[x].left && areaRight <= areaSelectDivObj[x].right && areaRight >= areaSelectDivObj[x].left) {
            //console.log('left out right in with '+x)
            conflictFlag = true
        }
        if (areaLeft <= areaSelectDivObj[x].left && areaRight >= areaSelectDivObj[x].left && areaRight >= areaSelectDivObj[x].right) {
            //console.log('left out and right out with '+x)
            conflictFlag = true
        }
        if (conflictFlag) {
            conflictsWith.push(x)
            if (areaLeft <= areaSelectDivObj[x].left && areaLeft <= min) {
                min = areaLeft
            }
            else if (areaSelectDivObj[x].left <= areaLeft && areaSelectDivObj[x].left <= min) {
                min = areaSelectDivObj[x].left
            }
            if (areaRight >= areaSelectDivObj[x].right && areaRight >= max) {
                max = areaRight
            }
            else if (areaSelectDivObj[x].right >= areaRight && areaSelectDivObj[x].right >= max) {
                max = areaSelectDivObj[x].right
            }
            if (type == 'on-cut') {
                $('.start-pin').css('left', min + 'px')
                $('.end-pin').css('left', max + 'px')
            }
            else if (type == 'on-drag-resize') {
                $(el).css('left', min + 'px')
                $(el).css('width', (max - min) + 'px')
            }
            for (index in conflictsWith) {
                $('#' + conflictsWith[index]).remove()
                delete areaSelectDivObj[conflictsWith[index]]
            }/*
			min = 999999999
			max = -1
			conflictFlag = false*/
        }
    }
    if (conflictFlag) {

    }

    if (areaSelectDivInserted)
        return true
    else
        return false
}



//code by girish
function checkMutedAreaSelectDivInsertion(type, el) {
    let areaLeft, areaRight
    if (type == 'on-mute') {
        areaLeft = parseInt($('.start-pin').css('left').split("px")[0])
        areaRight = parseInt($('.end-pin').css('left').split("px")[0])
    }
    else if (type == 'on-drag-resize') {
        areaLeft = parseInt($(el).css('left').split("px")[0])
        areaRight = parseInt($(el).css('left').split("px")[0]) + parseInt($(el).css('width').split("px")[0])
        trackId = el.id
        trackIdOfareaSelected = areaSelectMuteDivObj[trackId].trackID
    }


    let min = 999999999, max = -1

    let conflictFlag = false
    console.log("fsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdf");
    console.log(areaSelectMuteDivObj)
    for (let x in areaSelectMuteDivObj) {
        console.log(trackId)
        console.log(trackIdOfareaSelected)
        if (areaSelectMuteDivObj[x].trackID == trackIdOfareaSelected) {

            if (type == 'on-drag-resize' && el.id == x) {
                continue
            }
            conflictFlag = false
            console.log("adsasdasdasdasdasdasdasdasd")
            if (areaLeft >= areaSelectMuteDivObj[x].left && areaLeft <= areaSelectMuteDivObj[x].right && areaRight <= areaSelectMuteDivObj[x].right && areaRight >= areaSelectMuteDivObj[x].left) {
                //console.log('left in right in with '+x)
                conflictFlag = true
            }
            if (areaRight >= areaSelectMuteDivObj[x].right && areaLeft >= areaSelectMuteDivObj[x].left && areaLeft <= areaSelectMuteDivObj[x].right) {
                //console.log('left in and right out with '+x)
                conflictFlag = true
            }
            if (areaLeft <= areaSelectMuteDivObj[x].left && areaRight <= areaSelectMuteDivObj[x].right && areaRight >= areaSelectMuteDivObj[x].left) {
                //console.log('left out right in with '+x)
                conflictFlag = true
            }
            if (areaLeft <= areaSelectMuteDivObj[x].left && areaRight >= areaSelectMuteDivObj[x].left && areaRight >= areaSelectMuteDivObj[x].right) {
                //console.log('left out and right out with '+x)
                conflictFlag = true
            }
            if (conflictFlag) {
                conflictsWith.push(x)
                console.log(areaSelectMuteDivObj)
                if (areaLeft <= areaSelectMuteDivObj[x].left && areaLeft <= min) {
                    min = areaLeft
                }
                else if (areaSelectMuteDivObj[x].left <= areaLeft && areaSelectMuteDivObj[x].left <= min) {
                    min = areaSelectMuteDivObj[x].left
                }
                if (areaRight >= areaSelectMuteDivObj[x].right && areaRight >= max) {
                    max = areaRight
                }
                else if (areaSelectMuteDivObj[x].right >= areaRight && areaSelectMuteDivObj[x].right >= max) {
                    max = areaSelectMuteDivObj[x].right
                }
                if (type == 'on-mute') {
                    $('.start-pin').css('left', min + 'px')
                    $('.end-pin').css('left', max + 'px')
                }
                else if (type == 'on-drag-resize') {
                    $(el).css('left', min + 'px')
                    $(el).css('width', (max - min) + 'px')
                    changeId = $(el).attr("id")
                    console.log(changeId)
                    for (i = 0; i < muteDuration.length; i++) {
                        for (j = 0; j < muteDuration[i].length; j++) {
                            if (muteDuration[i][j] != undefined) {
                                if (muteDuration[i][j].AreaId == changeId) {
                                    console.log(muteDuration[i][j])
                                    console.log(min, max)
                                    muteDuration[i][j].ss = min / 10
                                    muteDuration[i][j].et = max / 10
                                    console.log(muteDuration[i][j])
                                    console.log(min / 10)
                                }
                            }

                        }

                    }
                    console.log(changeId)
                }
                for (index in conflictsWith) {
                    $('#' + conflictsWith[index]).remove()
                    delete areaSelectMuteDivObj[conflictsWith[index]]
                    console.log(index)
                    console.log(conflictsWith[index])
                    for (i = 0; i < muteDuration.length; i++) {
                        for (j = 0; j < muteDuration[i].length; j++) {
                            if (muteDuration[i][j] != undefined) {
                                if (muteDuration[i][j].AreaId == conflictsWith[index]) {
                                    delete muteDuration[i][j]
                                }
                            }
                        }

                    }
                }
                console.log(muteDuration)
                /*
                console
                min = 999999999
                max = -1
                conflictFlag = false*/
            }
        }
    }
    if (conflictFlag) {

    }

    if (areaSelectDivInserted)
        return true
    else
        return false
}
//
function updateAreaSelectDivObj(el, id) {
    areaSelectDivObj[id] =
        {
            left: parseInt(el.style.left.split('px')[0]),
            width: parseInt(el.style.width.split('px')[0]),
            right: parseInt(el.style.left.split("px")[0]) + parseInt(el.style.width.split("px")[0])
        }
}
function updateTrackAreaSelectDivObj(el, id) {
    console.log(id)
    areaSelectMuteDivObj[id].left = parseInt(el.style.left.split('px')[0])
    areaSelectMuteDivObj[id].width = parseInt(el.style.width.split('px')[0])
    areaSelectMuteDivObj[id].right = parseInt(el.style.left.split("px")[0]) + parseInt(el.style.width.split("px")[0])
}
function getAreaSelectDivObj() {
    return areaSelectDivObj
}
function getAreaSelectMuteDivObj() {
    return areaSelectMuteDivObj
}
module.exports = {
    drawRuler: drawRuler,
    handlePinMotion: handlePinMotion,
    animatePin: animatePin,
    getAreaSelectDivObj: getAreaSelectDivObj,
    cutPortion: cutPortion,
    setRulerWidth: setRulerWidth,
    getTrackContentID: getTrackContentID,
    mutePortion: mutePortion,
    getAreaSelectMuteDivObj: getAreaSelectMuteDivObj,
    process: process
};