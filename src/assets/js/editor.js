var child_process = require('child_process');
const { ipcRenderer, shell } = require("electron")
const electron = require("electron");
const { dialog } = require("electron").remote
var fs = require('fs')
//var configFilePath,configObj 
var spawn_proc_count
var child_process = require('child_process');
var exec = require('child_process').execSync;
var filesJs = require('./files.js')
var tracksJs = require('./tracks.js')


var cameraFiles = []
var screenFiles = []
var rimraf = require('rimraf');
var doneDuration = 0, totalDuration = 0
var overlayDurations = []
var overlayPcount = 0
var finalFilePath = ""
var configObj
var progress_width_preprocess = 0
var progress_width_overlay = 0
var progress_width_merge = 0
// progress bar variables
var a_progress_bar = "<div class='progress-bar-content'><h4 class='modal-title'>file_name_here</h4><div class='progress'><div id='id_here' class='progress-bar  active' role='progressbar' aria-valuenow='percent_complete' aria-valuemin=0 aria-valuemax=100 style=width:0%><span>0</span></div></div>";
var progress_bar_processing
var id_for_progresscheck = ""
var myid = ""
var _proc, procO, procArgs
var processes = []
var flagCancel					//flagCancel is for if it is 1 then processing cancel
var rawconfigObj = {}

function preprocess(configObject, configFilePath, areaObj, canvases) {
    flagCancel = 0
    spawn_proc_count = 0
    //configObj is updated but rawconfigObj is not
    rawconfigObj = JSON.parse(JSON.stringify(configObject));
    configObj = updateConfigObject(configObject, canvases, areaObj)
    configObj = getTrimTime(configObj);    // insert trim time in finalMerger
    configObj = getTrimCommand(configObj);     // insert trim command in finalMerger


    $("#the-log").html("")
    $(".back-btn").hide()
    $("#progress-text").html("Preprocessing Files ..")
    $("process-wrap").show()
    var allFileLen = tracksJs.getAllFileLengths();
    console.log('canvases : ', canvases, 'config : ', configObj, 'allFileLengths : ', allFileLen);
    var _args = [], _cmd
    var _args_screen = [], _cmd_screen
    var x = '', y = '', y_done = 0
    var background_img_path = ''

    debugger;
    for (let i = 0; i < configObj.finalMerger.length; i++) {
        console.log('i ', i)
        let obj = configObj.finalMerger[i]
        console.log(obj.type)
        console.log(obj);

    	/*background_img_path=$('#background').css('background-image')
    	background_img_path=background_img_path.replace('url("file:///','')
    	background_img_path=background_img_path.replace(')','')
    	background_img_path=background_img_path.replace('\"','')
    	background_img_path=background_img_path.replace('%20',' ')*/



        background_img_path = $('#background').css('background-image')
        if (background_img_path != 'none') {
            background_img_path = background_img_path.replace('url("file:///', '')
            background_img_path = background_img_path.replace(')', '')
            background_img_path = background_img_path.replace('\"', '')
            background_img_path = background_img_path.replace('%20', ' ')
        }



        if (obj.type == 'camera') {
            x = configFilePath + 'output/camera_new_' + (obj.cpath.split("/")[obj.cpath.split("/").length - 1]);
    		/*_args = [
    			'ffmpeg',
    			'-i' , configFilePath+obj.cpath,
    			'-i',background_img_path,
    			'-preset' , configObj.preset,
    			'-crf' , configObj.crf,
    			'-filter_complex', 
    			obj.trimCommand+'scale='+
    			obj.cwidth+':'+
    			obj.cheight+
    			',pad=width='+(obj.cwidth+obj.padding.paddingW)+
    			':height='+(obj.cheight + obj.padding.paddingH)+
    			':x='+(obj.padding.paddingLeft)+
    			':y='+(obj.padding.paddingTop)+
    			':color='+obj.padding.paddingColor+
    			'[in];[1]scale=1280:720[back];[back][in]overlay='+
    			(Math.round(obj.cleftper*12.8))+':'+(Math.round(obj.ctopper*7.2))+
    			',scale=1280:720,setsar=sar=1/1,setdar=dar=16/9'
    		];*/


            if (background_img_path != 'none') {
                _args = [
                    'ffmpeg',
                    '-i', configFilePath + obj.cpath,
                    '-i', background_img_path,
                    '-preset', configObj.preset,
                    '-crf', configObj.crf,
                    '-filter_complex',
                    obj.trimCommand + 'scale=' +
                    (obj.cwidth - obj.padding.paddingW) + ':' +
                    (obj.cheight - obj.padding.paddingH) +
                    ',pad=width=' + (obj.cwidth) +
                    ':height=' + (obj.cheight) +
                    ':x=' + (obj.padding.paddingLeft) +
                    ':y=' + (obj.padding.paddingTop) +
                    ':color=' + obj.padding.paddingColor +
                    '[in];[1]scale=' + (1280) + ':' + (720) + '[back];[back][in]overlay=' +
                    (Math.round(obj.cleftper * 12.8)) + ':' + (Math.round(obj.ctopper * 7.2)) +
                    ',setsar=sar=1/1,setdar=dar=16/9'
                ];
            }
            else {
                _args = [
                    'ffmpeg',
                    '-i', configFilePath + obj.cpath,
                    '-preset', configObj.preset,
                    '-crf', configObj.crf,
                    '-filter_complex',
                    obj.trimCommand + 'scale=' +
                    (obj.cwidth - obj.padding.paddingW) + ':' +
                    (obj.cheight - obj.padding.paddingH) +
                    ',pad=width=' + Math.round(obj.cwidth) +
                    ':height=' + Math.round(obj.cheight) +
                    ':x=' + Math.round(obj.padding.paddingLeft) +
                    ':y=' + Math.round(obj.padding.paddingTop) +
                    ':color=' + obj.padding.paddingColor +

                    ',pad=width=' + (1280) + ':height=' + (720) +
                    ':x=' + Math.round(obj.cleft) +
                    ':y=' + Math.round(obj.ctop) +
                    ':color=black,setsar=sar=1/1,setdar=dar=16/9'
                ];
            }
            obj.cpath = 'output/camera_new_' + (obj.cpath.split("/")[obj.cpath.split("/").length - 1]);

            if (obj.trimCommand == '[0]') {
                _args.push('-y', x)
            }
            else {
                _args[_args.length - 1] += ' [out3]'
                _args.push('-map', '[out3]',
                    '-map', '[aud]', '-y', x)
            }
            console.log(_args);
            debugger;
            //console.log(obj.trimCommand)
            //console.log(_args);
        }
        else if (obj.type == 'screen') {
            x = configFilePath + 'output/screen_new_' + (obj.cpath.split("/")[obj.cpath.split("/").length - 1]);

            /*_args = [
                'ffmpeg',
                '-i' , configFilePath+obj.cpath,
                '-i',background_img_path,
                '-crf' , configObj.crf,
                '-preset' , configObj.preset,
                '-filter_complex',
                obj.trimCommand+'crop='+(obj.windowCoords.width-20)+':'+(obj.windowCoords.height-25)+':'+(obj.windowCoords.screenX+10)+':'+(obj.windowCoords.screenY+10)+
                ',scale='+obj.swidth+':'+obj.sheight+
                ',pad=width='+(obj.swidth+obj.padding.paddingW)+
                ':height='+(obj.sheight + obj.padding.paddingH)+
                ':x='+(obj.padding.paddingLeft)+
                ':y='+(obj.padding.paddingTop)+
                ':color='+obj.padding.paddingColor+
                '[in];[1]scale=1280:720[back];[back][in]overlay='+
                Math.round(obj.sleftper*12.8)+':'+Math.round(obj.stopper*7.2)+
                ',setsar=sar=1/1,setdar=dar=16/9 ',

            ];*/

            //  for scale filter -> width is required to be divisible by 2
            var wDivBy2 = getDivBy2(obj.swidth + obj.padding.paddingW);
            var hDivBy2 = getDivBy2(obj.sheight + obj.padding.paddingH);
            if (background_img_path != 'none') {
                _args = [
                    'ffmpeg',
                    '-i', configFilePath + obj.cpath,
                    '-i', background_img_path,
                    '-crf', configObj.crf,
                    '-preset', configObj.preset,
                    '-filter_complex',
                    obj.trimCommand + 'crop=' + (obj.windowCoords.width - 20) + ':' + (obj.windowCoords.height - 25) + ':' + (obj.windowCoords.screenX + 10) + ':' + (obj.windowCoords.screenY + 10) +
                    ',scale=' +
                    (obj.swidth - obj.padding.paddingW) + ':' +
                    (obj.sheight - obj.padding.paddingH) +

                    ',pad=width=' + (obj.swidth) +
                    ':height=' + (obj.sheight) +
                    ':x=' + (obj.padding.paddingLeft) +
                    ':y=' + (obj.padding.paddingTop) +
                    ':color=' + obj.padding.paddingColor +
                    // '[in];[1]scale=' + wDivBy2 + ':' + hDivBy2 + '[back];[back][in]overlay=' +
                    '[in];[1]scale=' + (1280) + ':' + (720) + '[back];[back][in]overlay=' +
                    Math.round(obj.sleftper * 12.8) + ':' + Math.round(obj.stopper * 7.2) +
                    ',setsar=sar=1/1,setdar=dar=16/9 ',
                ];
            }
            else {
                _args = [
                    'ffmpeg',
                    '-i', configFilePath + obj.cpath,
                    '-crf', configObj.crf,
                    '-preset', configObj.preset,
                    '-filter_complex',
                    obj.trimCommand + 'crop=' + (obj.windowCoords.width - 20) + ':' + (obj.windowCoords.height - 25) + ':' + (obj.windowCoords.screenX + 10) + ':' + Math.round(obj.windowCoords.screenY + 10) +
                    ',scale=' +
                    (obj.swidth - obj.padding.paddingW) + ':' +
                    (obj.sheight - obj.padding.paddingH) +

                    ',pad=width=' + Math.round(obj.swidth) +
                    ':height=' + Math.round(obj.sheight) +

                    ':x=' + Math.round(obj.padding.paddingLeft) +
                    ':y=' + Math.round(obj.padding.paddingTop) +
                    ':color=' + obj.padding.paddingColor +

                    ',pad=width=' + (1280) + ':height=' + (720) +
                    ':x=' + Math.round(obj.sleft) +
                    ':y=' + Math.round(obj.stop) +
                    ':color=black,setsar=sar=1/1,setdar=dar=16/9'
                ];

            }


            obj.cpath = 'output/screen_new_' + (obj.cpath.split("/")[obj.cpath.split("/").length - 1]);

            if (obj.trimCommand == '[0]') {
                _args.push('-y', x)
            }
            else {

                _args[_args.length - 1] += ' [out2]'
                _args.push('-map', '[out2]',
                    '-map', '[aud]', '-y', x)
            }



        }
        else if (obj.type == 'both') {
            console.log('came to type = both', obj)
            x = configFilePath + 'output/camera_new_' + (obj.cpath.split("/")[obj.cpath.split("/").length - 1]);
            y = configFilePath + 'output/screen_new_' + (obj.spath.split("/")[obj.spath.split("/").length - 1]);


    		/*_args = [
    			'ffmpeg',
    			'-i' , configFilePath+obj.cpath,
    			'-preset' , configObj.preset,
    			'-crf' , configObj.crf,
    			'-filter_complex', 
    			obj.trimCommand[0].camera_trim+'scale='+obj.cwidth+':'+obj.cheight+
    			',pad=width='+Math.round(obj.cwidth+ obj.cpadding.paddingW)+
    			':height='+Math.round(obj.cheight + obj.cpadding.paddingH)+
    			//':height='+(Math.round(obj.cheightper*7.2) + Math.round(obj.padding.paddingH)*7.2)+
    			':x='+Math.round(obj.cpadding.paddingLeft)+
    			':y='+Math.round(obj.cpadding.paddingTop)+
    			':color='+obj.cpadding.paddingColor+','

    		];*/

            _args = [
                'ffmpeg',
                '-i', configFilePath + obj.cpath,
                '-preset', configObj.preset,
                '-crf', configObj.crf,
                '-filter_complex',
                obj.trimCommand[0].camera_trim + 'scale=' + Math.round(obj.cwidth - obj.cpadding.paddingW) + ':' + Math.round(obj.cheight - obj.cpadding.paddingH) +
                ',pad=width=' + Math.round(obj.cwidth) +
                ':height=' + Math.round(obj.cheight) +
                //':height='+(Math.round(obj.cheightper*7.2) + Math.round(obj.padding.paddingH)*7.2)+
                ':x=' + Math.round(obj.cpadding.paddingLeft) +
                ':y=' + Math.round(obj.cpadding.paddingTop) +
                ':color=' + obj.cpadding.paddingColor

            ];



            if (obj.trimCommand[0].camera_trim == '[0]') {
                _args.push('-y', x)
            }
            else {

                _args[_args.length - 1] += ' [out3]'
                _args.push('-map', '[out3]',
                    '-map', '[aud]', '-y', x)
            }
    		/*_args_screen = [
    				'ffmpeg',
    				'-i' , configFilePath+obj.spath,
    				'-i',background_img_path,
    				'-crf' , configObj.crf,
    				'-preset' , configObj.preset,
    				'-filter_complex',
    				obj.trimCommand[0].screen_trim+'crop='+
    					(obj.windowCoords.width-20)+':'+
    					(obj.windowCoords.height-25)+':'+
    					(obj.windowCoords.screenX+10)+':'+
    					(obj.windowCoords.screenY+10)+
    					',scale='+obj.swidth+
    					':'+obj.sheight+
    					',pad=width='+(obj.swidth+
    					obj.spadding.paddingLeft + obj.spadding.paddingRight)+
    					':height='+(obj.sheight + 
    					obj.spadding.paddingTop + obj.spadding.paddingBottom)+
    					':x='+obj.spadding.paddingLeft+
    					':y='+obj.spadding.paddingTop+
    					':color='+obj.spadding.paddingColor+
    					'[in];[1]scale=1280:720[back];[back][in]overlay='+
    					Math.round(obj.sleftper*12.8)+':'+
    					Math.round(obj.stopper*7.2)+
    					',setsar=sar=1/1,setdar=dar=16/9'
    			];*/


            if (background_img_path != 'none') {
                _args_screen = [
                    'ffmpeg',
                    '-i', configFilePath + obj.spath,
                    '-i', background_img_path,
                    '-crf', configObj.crf,
                    '-preset', configObj.preset,
                    '-filter_complex',
                    obj.trimCommand[0].screen_trim + 'crop=' +
                    (obj.windowCoords.width - 20) + ':' +
                    (obj.windowCoords.height - 25) + ':' +
                    (obj.windowCoords.screenX + 10) + ':' +
                    (obj.windowCoords.screenY + 10) +
                    ',scale=' +

                    (obj.swidth - (obj.spadding.paddingLeft + obj.spadding.paddingRight)) + ':' +
                    (obj.sheight - (obj.spadding.paddingTop + obj.spadding.paddingBottom)) +

                    ',pad=width=' + (obj.swidth) +
                    ':height=' + (obj.sheight) +

                    ':x=' + obj.spadding.paddingLeft +
                    ':y=' + obj.spadding.paddingTop +
                    ':color=' + obj.spadding.paddingColor +

                    '[in];[1]scale=' + (1280) + ':' + (720) + '[back];[back][in]overlay=' +
                    Math.round(obj.sleftper * 12.8) + ':' +
                    Math.round(obj.stopper * 7.2) +
                    ',setsar=sar=1/1,setdar=dar=16/9'
                ];
            }
            else {
                _args_screen = [
                    'ffmpeg',
                    '-i', configFilePath + obj.spath,
                    '-crf', configObj.crf,
                    '-preset', configObj.preset,
                    '-filter_complex',
                    obj.trimCommand[0].screen_trim + 'crop=' +
                    (obj.windowCoords.width - 20) + ':' +
                    (obj.windowCoords.height - 25) + ':' +
                    (obj.windowCoords.screenX + 10) + ':' +
                    (obj.windowCoords.screenY + 10) +
                    ',scale=' +
                    (obj.swidth - (obj.spadding.paddingLeft + obj.spadding.paddingRight)) + ':' +
                    (obj.sheight - (obj.spadding.paddingTop + obj.spadding.paddingBottom)) +

                    ',pad=width=' +
                    Math.round(obj.swidth) +
                    ':height=' + Math.round(obj.sheight) +

                    ':x=' + Math.round(obj.spadding.paddingLeft) +
                    ':y=' + Math.round(obj.spadding.paddingTop) +
                    ':color=' + obj.spadding.paddingColor +

                    ',pad=width=' + Math.round(1280) +
                    ':height=' + Math.round(720) +

                    ':x=' + Math.round(obj.sleft) +
                    ':y=' + Math.round(obj.stop) +

                    ':color=black,setsar=sar=1/1,setdar=dar=16/9'
                ];
            }






            if (obj.trimCommand[0].screen_trim == '[0]') {
                _args_screen.push('-y', y)
            }
            else {

                _args_screen[_args_screen.length - 1] += ' [out3]'
                _args_screen.push('-map', '[out3]',
                    '-map', '[aud]', '-y', y)
            }
            console.log('camera args : ', _args)
            console.log('screen args : ', _args_screen, obj)


            obj.cpath = 'output/camera_new_' + (obj.cpath.split("/")[obj.cpath.split("/").length - 1]);
            obj.spath = 'output/screen_new_' + (obj.spath.split("/")[obj.spath.split("/").length - 1]);

            _cmd_screen = _args_screen.shift();
            progress_bar_processing = a_progress_bar
            progress_bar_processing = progress_bar_processing.replace("file_name_here", obj.spath);
            var pre_id_replace = obj.spath.replace(/\//g, "");
            pre_id_replace = pre_id_replace.replace(/\./g, '');
            progress_bar_processing = progress_bar_processing.replace("id_here", pre_id_replace);
            $(".progress-bar-container").append(progress_bar_processing)

            _proc = child_process.spawn(_cmd_screen, _args_screen);
            processes.push(_proc)
            ipcRenderer.send('pid-message', _proc.pid);

            spawn_proc_count++;
            _proc.on('close', function (data) {
                // if flagCancel==1 then process is cancel
                if (flagCancel != 1) {
                    spawn_proc_count--;
                    id_for_progresscheck = obj.spath.replace(/\//g, "")
                    id_for_progresscheck = id_for_progresscheck.replace(/\./g, '')
                    myid = "#" + id_for_progresscheck
                    console.log(myid)
                    $(myid).html("<span>Preprocessing Complete!</span>");
                    $(myid).css('width', 100 + '%')

                    console.log("screen processing done")
                    //fs.renameSync(y,obj.spath);
                    $("#progress-text").html("Preprocessing Files .. <br />Remaining : " + spawn_proc_count)
                    console.log('files remaining : ', spawn_proc_count)
                    if (spawn_proc_count == 0) {
                        console.log('preprocessing done from inner')

                        replaceBeforeOverlay(configFilePath)

                        //startOverlaying(configFilePath)
                    }
                }
            })
            _proc.stderr.on('data', (data) => {
                if (data.toString().includes("time=")) {
                    let x1 = ((data.toString().split("time=")[1]).split(" ")[0]).split(":")
                    let hrs = parseInt(x1[0])
                    let mins = parseInt(x1[1])
                    let secs = parseFloat(parseFloat(x1[2]).toFixed(2))
                    doneDuration = (hrs * 60) + (mins * 60) + secs
                    console.log(doneDuration)
                    console.log(((doneDuration / (configObj.finalMerger[i].endTime - configObj.finalMerger[i].startTime)) * 100).toFixed(0))
                    id_for_progresscheck = obj.spath.replace(/\//g, "")
                    id_for_progresscheck = id_for_progresscheck.replace(/\./g, '')
                    myid = "#" + id_for_progresscheck
                    console.log(myid)
                    progress_width_preprocess = (((doneDuration / (configObj.finalMerger[i].endTime - configObj.finalMerger[i].startTime)) * 100).toFixed(0))
                    $(myid).html("<span>" + progress_width_preprocess + "%</span>");
                    $(myid).css('width', progress_width_preprocess + '%')
                    console.log(obj)
                }
                console.log('problem with i : ', i, 'data', data.toString())
                $("#the-log").append("<div class='a-log'>" + data.toString() + "</div>")
                $("#the-log").scrollTop($("#the-log").prop("scrollHeight"))
            })
        }
        _cmd = _args.shift();
        progress_bar_processing = a_progress_bar
        //filename & id 
        if (obj.type == 'screen' && obj.cpath == undefined) {
            progress_bar_processing = progress_bar_processing.replace("file_name_here", obj.spath);
            var pre_id_replace = obj.spath.replace(/\//g, "");
            pre_id_replace = pre_id_replace.replace(/\./g, "")
            progress_bar_processing = progress_bar_processing.replace("id_here", pre_id_replace);

        }
        else if (obj.type == 'screen' && obj.spath == undefined) {
            progress_bar_processing = progress_bar_processing.replace("file_name_here", obj.cpath);
            var pre_id_replace = obj.cpath.replace(/\//g, "");
            pre_id_replace = pre_id_replace.replace(/\./g, "")
            progress_bar_processing = progress_bar_processing.replace("id_here", pre_id_replace);
        }
        else {
            progress_bar_processing = progress_bar_processing.replace("file_name_here", obj.cpath);
            var pre_id_replace = obj.cpath.replace(/\//g, "");
            pre_id_replace = pre_id_replace.replace(/\./g, '');
            progress_bar_processing = progress_bar_processing.replace("id_here", pre_id_replace);
        }
        $(".progress-bar-container").append(progress_bar_processing)

        _proc = child_process.spawn(_cmd, _args);
        processes.push(_proc)
        ipcRenderer.send('pid-message', _proc.pid);
        spawn_proc_count++;
        _proc.stderr.on('data', (data) => {
            if (data.toString().includes("time=")) {
                let x1 = ((data.toString().split("time=")[1]).split(" ")[0]).split(":")
                let hrs = parseInt(x1[0])
                let mins = parseInt(x1[1])
                let secs = parseFloat(parseFloat(x1[2]).toFixed(2))
                doneDuration = (hrs * 60) + (mins * 60) + secs
                console.log(doneDuration)
                console.log(((doneDuration / (configObj.finalMerger[i].endTime - configObj.finalMerger[i].startTime)) * 100).toFixed(0))

                id_for_progresscheck = obj.cpath.replace(/\//g, "")
                id_for_progresscheck = id_for_progresscheck.replace(/\./g, '')
                myid = "#" + id_for_progresscheck
                console.log(myid)
                progress_width_preprocess = (((doneDuration / (configObj.finalMerger[i].endTime - configObj.finalMerger[i].startTime)) * 100).toFixed(0))
                $(myid).html("<span>" + progress_width_preprocess + "%</span>");
                $(myid).css('width', progress_width_preprocess + '%')
                console.log(obj)
            }

            console.log(data.toString())
            $("#the-log").append("<div class='a-log'>" + data.toString() + "</div>")
            $("#the-log").scrollTop($("#the-log").prop("scrollHeight"))
        })
        _proc.on('close', function (data) {
            // if flagCancel==1 then process is cancel
            if (flagCancel != 1) {
                if (data != null) {
                    $("#the-log").append("<div class='a-log'>" + data.toString() + "</div>")
                }
                $("#the-log").scrollTop($("#the-log").prop("scrollHeight"))
                spawn_proc_count--;
                //rimraf.sync(fp2);
                console.log('files remaining : ', spawn_proc_count)
                $("#progress-text").html("Preprocessing Files .. <br />Remaining : " + spawn_proc_count)
                //fs.renameSync(x,fp2);
                //$("#progress-text").html("Preprocessing Files , <br> remaining  : "+(parseInt(spawn_proc_count)+1))
                //console.log('closed a process! '+spawn_proc_count+" private flag : ",privateStopFlag);
                //fs.renameSync(x,obj.cpath);	
                id_for_progresscheck = obj.cpath.replace(/\//g, "")
                id_for_progresscheck = id_for_progresscheck.replace(/\./g, '')
                myid = "#" + id_for_progresscheck
                console.log(myid)
                $(myid).html("<span>Preprocessing Complete!</span>");
                $(myid).css('width', 100 + '%')

                if (spawn_proc_count == 0) {
                    console.log('preprocessing done from outer')
                    replaceBeforeOverlay(configFilePath)

                    // startOverlaying(configFilePath)
                }
            }
        }, i);
    }
}

function startOverlaying(configFilePath) {
    screenFiles = [];
    cameraFiles = [];
    overlayPcount = 0
	/*if (fs.existsSync('./mergingList.txt'))
	{
		fs.truncateSync('./mergingList.txt');
    }*/

	/*var pos ="";
	if(configObj.overlayPos == "bl")
		pos = "(0):(H-h)";
	if(configObj.overlayPos == "br")
		pos = "(W-w):(H-h)";
	if(configObj.overlayPos == "tl")
		pos = "(0):(0)";
	if(configObj.overlayPos == "tr")
		pos = "(W-w):(0)";
	if(configObj.overlayPos == "hidden")
		pos = "hidden";*/

    // handle if there exists only one object in finalMerger

    console.log('finalMerger : ', configObj.finalMerger)
    let both = false;

    for (let i = 0; i < configObj.finalMerger.length; i++) {
        if (configObj.finalMerger[i].type == 'both') {
            both = true;
            let outp = (configObj.finalMerger[i].cpath.split('/'))[(configObj.finalMerger[i].cpath.split('/')).length - 1];
            console.log('values in videoUtils')
            console.log(configObj.transparencySimilarity, ":", configObj.transparencyOpacity)

            var overArgs = []
			/*if(configObj.transparencyCheck)
			{
				overArgs = [
					'ffmpeg',
					'-i' , configFilePath+configObj.finalMerger[i].spath,
					'-i' , configFilePath+configObj.finalMerger[i].cpath,
					'-r' , '30',
					'-s' , '1280x720',
					'-filter_complex','[0]fps=30 [back];[1:v]chromakey=0x'+configObj.colorHex+':'+(configObj.transparencySimilarity)+':'+(configObj.transparencyOpacity)+',scale='+configObj.overlayRes+'[ckout];[back][ckout]overlay='+configObj.finalMerger[i].cleft+':'+configObj.finalMerger[i].ctop+':shortest=1,setsar=sar=1/1,setdar=dar=16/9[out]',
					'-map', '[out]',
					'-map', '1:a',
					'-codec:a', 'copy',
					'-y', configFilePath+'/camera/new_'+outp
				];		
			}
			else
			{
				
				overArgs = [
					'ffmpeg',
					'-i',configFilePath+configObj.finalMerger[i].cpath,
					'-i',configFilePath+configObj.finalMerger[i].spath,
					'-filter_complex', '[0]scale='+configObj.finalMerger[i].cwidth+':'+configObj.finalMerger[i].cheight+'[front];[1]crop='+(configObj.finalMerger[i].windowCoords.width-20)+':'+(configObj.finalMerger[i].windowCoords.height-25)+':'+(configObj.finalMerger[i].windowCoords.screenX+10)+':'+(configObj.finalMerger[i].windowCoords.screenY+12)+',fps=30[back];[back][front]overlay='+parseInt(configObj.finalMerger[i].cleftper * (1280/100))+':'+parseInt(configObj.finalMerger[i].ctopper * (720/100))+',scale=1280:720,setsar=sar=1/1,setdar=dar=16/9',
					'-y', configFilePath+'/camera/new_'+outp,
				]
				console.log('overArgs : ',overArgs,' configObject : ',configObj);
				
			}*/
            console.log(configObj);


            // @ patch
            // @author vedang
            // @reason : in case of transparency not applied . this line throws error
            //if(configObj.finalMerger[i].transparency == undefined){
            //configObj.finalMerger[i].transparency = {transparecyCheck : false};
            //}
            console.log(configObj);
            if (configObj.finalMerger[i].transparency.transparencyCheck) {

                console.log('hello')
                overArgs = [
                    'ffmpeg',
                    '-i', configFilePath + configObj.finalMerger[i].spath,
                    '-i', configFilePath + configObj.finalMerger[i].cpath,
                    '-r', '30',
                    '-s', '1280x720',
                    '-filter_complex', '[0]fps=30 [back];[1:v]chromakey=0x' + configObj.finalMerger[i].transparency.colorHex + ':' + (configObj.finalMerger[i].transparency.transparencySimilarity) + ':' + (configObj.finalMerger[i].transparency.transparencyOpacity)
                    + '[ckout];[back][ckout]overlay='
                    + Math.round(configObj.finalMerger[i].cleft) + ':'
                    + Math.round(configObj.finalMerger[i].ctop)

                    + ':shortest=1,setsar=sar=1/1,setdar=dar=16/9[out]',
                    '-map', '[out]',
                    '-map', '1:a',
                    '-codec:a', 'copy',
                    '-y', configFilePath + 'output/overlay_new_' + outp
                ];
                console.log(overArgs)

            }

            else {

				/*overArgs = [
					'ffmpeg',
					'-i',configFilePath+configObj.finalMerger[i].cpath,
					'-i',configFilePath+configObj.finalMerger[i].spath,
					'-filter_complex', 
					'[0]scale='
					+(Math.round((configObj.finalMerger[i].cwidth +configObj.finalMerger[i].cpadding.paddingW ))) +':'
					+(Math.round((configObj.finalMerger[i].cheight + configObj.finalMerger[i].cpadding.paddingH )))
					+'[front];[1]crop='+(configObj.finalMerger[i].windowCoords.width-20)
					+':'+(configObj.finalMerger[i].windowCoords.height-25)
					+':'+(configObj.finalMerger[i].windowCoords.screenX+10)
					+':'+(configObj.finalMerger[i].windowCoords.screenY+12)
					+',scale=1280:720,fps=30[back];[back][front]overlay='
					+Math.round(configObj.finalMerger[i].cleftper *12.8)+':'
					+Math.round(configObj.finalMerger[i].ctopper *7.2)
					+',setsar=sar=1/1,setdar=dar=16/9',
					'-y', configFilePath+'/camera/new_'+outp,
				];*/

                /* overArgs = [
                     'ffmpeg',
                     '-i', configFilePath + configObj.finalMerger[i].cpath,
                     '-i', configFilePath + configObj.finalMerger[i].spath,
                     '-filter_complex',
                     '[0]scale='
                     + (Math.round((configObj.finalMerger[i].cwidth + configObj.finalMerger[i].cpadding.paddingW))) + ':'
                     + (Math.round((configObj.finalMerger[i].cheight + configObj.finalMerger[i].cpadding.paddingH)))
                     + '[front];[1]fps=30[back];[back][front]overlay='
                     + Math.round(configObj.finalMerger[i].cleft) + ':'
                     + Math.round(configObj.finalMerger[i].ctop)
                     + ',setsar=sar=1/1,setdar=dar=16/9',
                     '-y', configFilePath + 'output/overlay_new_' + outp,
                 ];*/
                overArgs = [
                    'ffmpeg',
                    '-i', configFilePath + configObj.finalMerger[i].cpath,
                    '-i', configFilePath + configObj.finalMerger[i].spath,
                    '-filter_complex',
					/*'[0]scale='
					+(Math.round((configObj.finalMerger[i].cwidth +configObj.finalMerger[i].cpadding.paddingW )-(configObj.finalMerger[i].spadding.paddingLeft + configObj.finalMerger[i].spadding.paddingRight))) +':'
					+(Math.round((configObj.finalMerger[i].cheight + configObj.finalMerger[i].cpadding.paddingH )-(configObj.finalMerger[i].spadding.paddingTop + configObj.finalMerger[i].spadding.paddingBottom)))
					+'[front];*/

                    '[1]fps=30[back];[back][0]overlay='
                    + Math.round(configObj.finalMerger[i].cleft) + ':'
                    + Math.round(configObj.finalMerger[i].ctop)
                    + ',setsar=sar=1/1,setdar=dar=16/9',
                    '-y', configFilePath + 'output/overlay_new_' + outp,
                ];
                console.log('overArgs : ', overArgs, ' configObject : ', configObj);



            }
            configObj.finalMerger[i].cpath = 'output/overlay_new_' + outp;
            var cmdO = overArgs.shift();
            procO = child_process.spawn(cmdO, overArgs);
            progress_bar_processing = a_progress_bar
            var overlay_spath = configObj.finalMerger[i].spath.replace(/\//g, "")
            var overlay_cpath = configObj.finalMerger[i].cpath.replace(/\//g, "")
            var overlay_id_replace = overlay_cpath + overlay_spath
            overlay_id_replace = overlay_id_replace.replace(/\./g, "")
            progress_bar_processing = progress_bar_processing.replace("file_name_here", "Overlay_processing_of_ " + configObj.finalMerger[i].spath + configObj.finalMerger[i].cpath)
            progress_bar_processing = progress_bar_processing.replace("id_here", overlay_id_replace);
            $(".progress-bar-container").append(progress_bar_processing);

            ipcRenderer.send('pid-message', procO.pid);
            overlayPcount++;
            overlayDurations[i] = {};
            procO.on('close', function () {
                // if flagCancel==1 then process is cancel
                if (flagCancel != 1) {
                    overlayPcount--
                    console.log('overlayPcount : ', overlayPcount)

                    var overlay_spath = configObj.finalMerger[i].spath.replace(/\//g, "")
                    var overlay_cpath = configObj.finalMerger[i].cpath.replace(/\//g, "")
                    var overlay_id_replace = overlay_cpath + overlay_spath
                    overlay_id_replace = "#" + overlay_id_replace.replace(/\./g, "")
                    $(overlay_id_replace).html("<span>Overlay Complete!</span>");
                    $(overlay_id_replace).css('width', 100 + '%')

                    console.log('overlayPcount : ', overlayPcount)
                    if (overlayPcount == 0) {
                        replaceNews(configFilePath);

                        finalMerge(configFilePath);
                    }
                }
            })
            procO.stderr.on('data', function (data) {
                console.log(data.toString())
                $("#the-log").append("<div class='a-log'>" + data.toString() + "</div>")
                $("#the-log").scrollTop($("#the-log").prop("scrollHeight"))
                if (data.toString().includes("time=")) {
                    let otime = data.toString().split("time=")[1].split(" ")[0].split(":")
                    let ohrs = parseInt(otime[0])
                    let omins = parseInt(otime[1])
                    let osecs = parseFloat(parseFloat(otime[2]).toFixed(2))
                    let oTimeSecs = (ohrs * 60) + (omins * 60) + osecs
                    //console.log('OTIME : ',oTimeSecs,' of video no : ',i)

                    overlayDurations[i].completedDuration = oTimeSecs;

                    //console.log("For video ",i,' Completed : ',overlayDurations[i].completedDuration,' Total : ',overlayDurations[i].totalDuration , "Percent : ", (overlayDurations[i].completedDuration / overlayDurations[i].totalDuration*100))

                    progress_width_overlay = ((overlayDurations[i].completedDuration / overlayDurations[i].totalDuration * 100).toFixed(0))

                    var overlay_spath = configObj.finalMerger[i].spath.replace(/\//g, "")
                    var overlay_cpath = configObj.finalMerger[i].cpath.replace(/\//g, "")
                    var overlay_id_replace = overlay_cpath + overlay_spath
                    overlay_id_replace = "#" + overlay_id_replace.replace(/\./g, "")
                    $(overlay_id_replace).css('width', progress_width_overlay + '%')
                    $(overlay_id_replace).html("<span>" + progress_width_overlay + "%</span>");
                    console.log(overlay_id_replace)
                    console.log(progress_width_overlay)


                    let x = 0;
                    for (let n = 0; n < overlayDurations.length; n++) {
                        if (overlayDurations[n] != undefined)
                            x = x + ((overlayDurations[n].completedDuration) / (overlayDurations[n].totalDuration) * 100)
                    }
                    if (!isNaN((x / 2).toFixed(0))) {
                        //console.log("Combined Overlay % : ", (x/(overlayDurations.length)).toFixed(0))
                        $("#progress-text").html("Mixing camera and screen ...")
                        $("#rec-progress-bar").css('width', (x / (overlayDurations.length)).toFixed(0) + "%")
                        //$("#rec-progress-bar").attr("aria-valuenow",(x/(overlayDurations.length)).toFixed(0))
                    }
                }
                else if (data.toString().includes("Duration")) {
                    let odur = data.toString().split("Duration:")[1].split(",")[0].split(":")
                    console.log(odur)
                    let odurhrs = parseInt(odur[0])
                    let odurmins = parseInt(odur[1])
                    let odursecs = parseFloat(parseFloat(odur[2]).toFixed(2))
                    let odurTimeSecs = (odurhrs * 60) + (odurmins * 60) + odursecs

                    console.log('overlay Duration : ', overlayDurations, ' i : ', i)
                    overlayDurations[i].totalDuration = odurTimeSecs;
                    //console.log('ODURATION : ',odurTimeSecs,' of video no : ',i)
                }
            }, i);
        }
    }

    if (both == false) {
        // if flagCancel==1 then process is cancel
        if (flagCancel != 1)
            finalMerge();
    }
}


function getTrimTime(configObj) {
    console.log(configObj.cutArea)


    for (let i = 0; i < configObj.finalMerger.length; i++) {
        configObj.finalMerger[i].trim = [];

        for (let j = 0; j < configObj.cutArea.length; j++) {
            //If a cut started before video starts and ends before video ends
            if (configObj.cutArea[j].start < configObj.finalMerger[i].startTime && configObj.cutArea[j].end > configObj.finalMerger[i].startTime) {
                console.log('If a cut started before video starts and ends before video ends')
                var temp;
                if (j != configObj.cutArea.length - 1 && configObj.cutArea[j + 1].start < configObj.finalMerger[i].endTime) {
                    temp = configObj.cutArea[j + 1].start;
                }
                else {
                    temp = configObj.finalMerger[i].endTime;
                }

                configObj.finalMerger[i].trim.push({
                    start: configObj.cutArea[j].end - configObj.finalMerger[i].startTime,
                    end: temp - configObj.finalMerger[i].startTime
                });
                console.log('start : ')
            }

            //if a cut starts at video start time
            else if (configObj.cutArea[j].start == configObj.finalMerger[i].startTime) {
                console.log('if a cut starts at video start time')
                var temp1;
                if (configObj.cutArea[j].end < configObj.finalMerger[i].endTime) {
                    temp1 = configObj.cutArea[j].end;
                }
                else {

                }

                var temp2;
                if (j != configObj.cutArea.length - 1 && configObj.cutArea[j + 1].start < configObj.finalMerger[i].endTime) {
                    temp2 = configObj.cutArea[j + 1].start;
                }
                else {
                    temp2 = configObj.finalMerger[i].endTime;
                }


                configObj.finalMerger[i].trim.push({
                    start: temp1 - configObj.finalMerger[i].startTime,
                    end: temp2 - configObj.finalMerger[i].startTime
                });
            }
            //If a cut start after video start time and ends before endtime
            else if (configObj.cutArea[j].start > configObj.finalMerger[i].startTime && configObj.cutArea[j].end < configObj.finalMerger[i].endTime) {
                console.log('If a cut start after video start time and ends before endtime')
                //If it is the first cut
                if (j == 0) {
                    configObj.finalMerger[i].trim.push({
                        start: 0,
                        end: configObj.cutArea[j].start - configObj.finalMerger[i].startTime
                    });
                }//if its video's first cut
                else if (configObj.cutArea[j - 1].end < configObj.finalMerger[i].startTime) {
                    configObj.finalMerger[i].trim.push({
                        start: 0,
                        end: configObj.cutArea[j].start - configObj.finalMerger[i].startTime
                    });
                }

                var temp2;
                if (j != configObj.cutArea.length - 1 && configObj.cutArea[j + 1].start < configObj.finalMerger[i].endTime) {
                    temp2 = configObj.cutArea[j + 1].start;
                }
                else {
                    temp2 = configObj.finalMerger[i].endTime;
                }


                configObj.finalMerger[i].trim.push({
                    start: configObj.cutArea[j].end - configObj.finalMerger[i].startTime,
                    end: temp2 - configObj.finalMerger[i].startTime
                });
            }
            //If a cut start after video start time and ends after endtime or at the same time of video end
            else if (configObj.cutArea[j].start > configObj.finalMerger[i].startTime && configObj.cutArea[j].end >= configObj.finalMerger[i].endTime) {
                console.log('If a cut start after video start time and ends after endtime or at the same time of video end')
                //If it is the first cut
                if (j == 0) {
                    configObj.finalMerger[i].trim.push({
                        start: 0,
                        end: configObj.cutArea[j].start - configObj.finalMerger[i].startTime
                    });
                }//if its video's first cut
                else if (configObj.cutArea[j - 1].end < configObj.finalMerger[i].startTime) {
                    configObj.finalMerger[i].trim.push({
                        start: 0,
                        end: configObj.cutArea[j].start - configObj.finalMerger[i].startTime
                    });
                }
            }

        }

    }

    console.log(configObj);
    return configObj
}





function getTrimCommand(configObj) {
    for (let i = 0; i < configObj.finalMerger.length; i++) {
        let obj = configObj.finalMerger[i]
        var command = '[0]';
        let streamMapping = '-map [out1] -map [aud]';

        if (obj.type == 'camera') {
            if (obj.trim.length == 0) {
                streamMapping = '';
            }
            else if (obj.trim.length == 1) {
                command += 'trim=start=' + obj.trim[0].start + ':end=' + obj.trim[0].end + ',setpts=PTS-STARTPTS[out1];[0]atrim=start=' + obj.trim[0].start + ':end=' + obj.trim[0].end + ',asetpts=PTS-STARTPTS[aud];[out1]';
            }
            else {
                for (let k = 0; k < obj.trim.length; k++) {
                    if (k == 0) {
                        command += 'trim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',setpts=PTS-STARTPTS[v' + k + '];[0]atrim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',asetpts=PTS-STARTPTS[a' + k + '];';
                    }

					/*if(k == 1)
					{
						command += '[0]trim=start'+obj.trim[k]startTime+':end='+obj.trim[k]endTime+',setpts=PTS-STARTPTS[v'+k+'];[0]atrim=start'+obj.trim[k]startTime+':end='+obj.trim[k]endTime+',asetpts=PTS-STARTPTS[a'+k+'];';
					
						vidConcat += '[v'+k+'] ';
						audConcat += '[a'+a+'] ';
					}*/

                    if (k > 0) {
                        command += '[0]trim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',setpts=PTS-STARTPTS[v' + k + '];[0]atrim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',asetpts=PTS-STARTPTS[a' + k + '];';

                        if (k == obj.trim.length - 1) {
                            command += '[v' + (k - 1) + '][v' + k + '] concat[out1];'
                            command += '[a' + (k - 1) + '][a' + k + '] concat=v=0:a=1[aud];[out1]'
                        }
                        else {
                            command += '[v' + (k - 1) + '][v' + k + '] concat[v' + k + '];'
                            command += '[a' + (k - 1) + '][a' + k + '] concat=v=0:a=1[a' + k + '];'
                        }

                    }
                }


            }

            configObj.finalMerger[i].trimCommand = command;
            configObj.finalMerger[i].mappingCommand = streamMapping;

        }
        else if (obj.type == 'screen') {
            if (obj.trim.length == 0) {
                streamMapping = '';
            }
            else if (obj.trim.length == 1) {
                command += 'trim=start=' + obj.trim[0].start + ':end=' + obj.trim[0].end + ',setpts=PTS-STARTPTS[out1];[0]atrim=start=' + obj.trim[0].start + ':end=' + obj.trim[0].end + ',asetpts=PTS-STARTPTS[aud];[out1]';
            }
            else {
                for (let k = 0; k < obj.trim.length; k++) {
                    if (k == 0) {
                        command += 'trim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',setpts=PTS-STARTPTS[v' + k + '];[0]atrim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',asetpts=PTS-STARTPTS[a' + k + '];';
                    }

					/*if(k == 1)
					{
						command += '[0]trim=start'+obj.trim[k]startTime+':end='+obj.trim[k]endTime+',setpts=PTS-STARTPTS[v'+k+'];[0]atrim=start'+obj.trim[k]startTime+':end='+obj.trim[k]endTime+',asetpts=PTS-STARTPTS[a'+k+'];';
					
						vidConcat += '[v'+k+'] ';
						audConcat += '[a'+a+'] ';
					}*/

                    if (k > 0) {
                        command += '[0]trim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',setpts=PTS-STARTPTS[v' + k + '];[0]atrim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',asetpts=PTS-STARTPTS[a' + k + '];';

                        if (k == obj.trim.length - 1) {
                            command += '[v' + (k - 1) + '][v' + k + '] concat[out1];'
                            command += '[a' + (k - 1) + '][a' + k + '] concat=v=0:a=1[aud];[out1]'
                        }
                        else {
                            command += '[v' + (k - 1) + '][v' + k + '] concat[v' + k + '];'
                            command += '[a' + (k - 1) + '][a' + k + '] concat=v=0:a=1[a' + k + '];'
                        }

                    }
                }


            }

            configObj.finalMerger[i].trimCommand = command;
            configObj.finalMerger[i].mappingCommand = streamMapping;

        }
        else if (obj.type == 'both') {
            var c_command = '[0]';
            var s_command = '[0]';

            if (obj.trim.length == 0) {
                streamMapping = '';
            }
            else if (obj.trim.length == 1) {
                c_command += 'trim=start=' + obj.trim[0].start + ':end=' + obj.trim[0].end + ',setpts=PTS-STARTPTS[out1];[0]atrim=start=' + obj.trim[0].start + ':end=' + obj.trim[0].end + ',asetpts=PTS-STARTPTS[aud];[out1]';

                s_command += 'trim=start=' + obj.trim[0].start + ':end=' + obj.trim[0].end + ',setpts=PTS-STARTPTS[out1];[0]atrim=start=' + obj.trim[0].start + ':end=' + obj.trim[0].end + ',asetpts=PTS-STARTPTS[aud];[out1]';
            }
            else {
                for (let k = 0; k < obj.trim.length; k++) {
                    if (k == 0) {
                        c_command += 'trim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',setpts=PTS-STARTPTS[v' + k + '];[0]atrim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',asetpts=PTS-STARTPTS[a' + k + '];';

                        s_command += 'trim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',setpts=PTS-STARTPTS[v' + k + '];[0]atrim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',asetpts=PTS-STARTPTS[a' + k + '];';
                    }

					/*if(k == 1)
					{
						command += '[0]trim=start'+obj.trim[k]startTime+':end='+obj.trim[k]endTime+',setpts=PTS-STARTPTS[v'+k+'];[0]atrim=start'+obj.trim[k]startTime+':end='+obj.trim[k]endTime+',asetpts=PTS-STARTPTS[a'+k+'];';
					
						vidConcat += '[v'+k+'] ';
						audConcat += '[a'+a+'] ';
					}*/

                    if (k > 0) {
                        c_command += '[0]trim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',setpts=PTS-STARTPTS[v' + k + '];[0]atrim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',asetpts=PTS-STARTPTS[a' + k + '];';

                        s_command += '[0]trim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',setpts=PTS-STARTPTS[v' + k + '];[0]atrim=start=' + obj.trim[k].start + ':end=' + obj.trim[k].end + ',asetpts=PTS-STARTPTS[a' + k + '];';

                        if (k == obj.trim.length - 1) {
                            c_command += '[v' + (k - 1) + '][v' + k + '] concat[out1];'
                            c_command += '[a' + (k - 1) + '][a' + k + '] concat=v=0:a=1[aud];[out1]'

                            s_command += '[v' + (k - 1) + '][v' + k + '] concat[out1];'
                            s_command += '[a' + (k - 1) + '][a' + k + '] concat=v=0:a=1[aud];[out1]'
                        }
                        else {
                            c_command += '[v' + (k - 1) + '][v' + k + '] concat[v' + k + '];'
                            c_command += '[a' + (k - 1) + '][a' + k + '] concat=v=0:a=1[a' + k + '];'

                            s_command += '[v' + (k - 1) + '][v' + k + '] concat[v' + k + '];'
                            s_command += '[a' + (k - 1) + '][a' + k + '] concat=v=0:a=1[a' + k + '];'
                        }

                    }
                }


            }

            configObj.finalMerger[i].trimCommand = [];

            configObj.finalMerger[i].trimCommand.push({
                camera_trim: c_command,
                screen_trim: s_command
            });
            configObj.finalMerger[i].mappingCommand = streamMapping;

        }

    }

    return configObj;
}

function replaceNews(configFilePath) {
	/*console.log('came to replace')
	var newFiles = [],oldFiles = []
	fs.readdirSync(configFilePath+'/camera').forEach(file => {
			if(file.includes("new_"))
				newFiles.push(configFilePath+'/camera/'+file)
			else
			{
				oldFiles.push(configFilePath+'/camera/'+file)
			}
	})
	console.log('new : ',newFiles,'old : ',oldFiles)
	for(let i=0;i<newFiles.length;i++)
	{
		let x = newFiles[i].split("_")[newFiles[i].split("_").length - 1];	
		for(let j=0;j<oldFiles.length;j++)
		{
			if(oldFiles[j].includes(x))
			{
				console.log('in the replacer')
				fs.unlinkSync(oldFiles[j])
				fs.renameSync(newFiles[i],oldFiles[j])
			}
		}
	}*/
    getTotalDuration(configFilePath)
}
function getTotalDuration(configFilePath) {
    let duration = 0.0;
    //console.log('came to get totalDuration')
    var testFiles = []
    fs.readdirSync(configFilePath + '/camera/').forEach(file => {
        testFiles.push(configFilePath + '/camera/' + file)
    })
    console.log('testFiles  : ', testFiles)
    for (let i = 0; i < testFiles.length; i++) {
        var argsDur = [
            'ffprobe',
            '-v', 'error',
            '-show_entries',
            'format=duration',
            testFiles[i],
        ];
        var cmdDur = argsDur.shift();
        var procDur = child_process.spawn(cmdDur, argsDur);
        ipcRenderer.send('process-spawn', procDur.pid)
        procDur.stderr.on('data', (data) => {
            /*			//console.log(data.toString())*/
        })
        procDur.stdout.on('data', (data) => {
            if (data.includes("duration")) {
                let x = parseFloat(parseFloat(data.toString().split("duration=")[1].split("[")[0].trim(" ")).toFixed(2));
                duration = duration + x;
                //console.log('Duration : ',duration)
            }
        })
        procDur.on('close', function (data) {
            if (testFiles.length - i == 1) {
                console.log('Total Duration in camera folder : ', duration)
                totalDuration = (duration);
            }
        })
    }
}
function finalMerge(configFilePath) {
    console.log('finalMerger : ', configObj.finalMerger)
    var totalTime = 0
    for (let i = 0; i < (configObj.finalMerger.length); i++) {
        var xx = configObj.finalMerger[i].cpath + "\n";
        var txt = (xx.split("/")[xx.split("/").length - 2]) + "/" + (xx.split("/")[xx.split("/").length - 1])
        fs.appendFileSync(configFilePath + '/mergingList.txt', ("file ") + txt);
        totalTime = totalTime + (configObj.finalMerger[i].endTime - configObj.finalMerger[i].startTime)

    }
    //console.log('after reading from directory : ');
    /*var optname = configFilePath+'/'+configFilePath+'.mp4'*/
    optname = configFilePath + '.mp4';
    console.log('OUTPUT NAME : ', optname)

    if (configObj.finalMerger.length == 1) {

        $("#progress-text").text("Video generated successfully !")
        $(".progress").hide()
        $(".show-file-wrap").show()
        $(".back-btn").show()
        ipcRenderer.send('clear-for-both');


        //alert('File Generated - ' + configFilePath + configObj.finalMerger[0].cpath)

        $("#progress-text").text("")
        $(".progress").hide()
        swal({
            title: "File Generated - " + configFilePath + configObj.finalMerger[0].cpath,
            text: "Click to close button!",
            icon: "success",
            button: "Close!",
        })
        //remove progress-bar divs
        remove_Progress_div();
        //hide modal after processing done! 
        $('#saveModal').modal('hide')
        $('.modal-backdrop').hide()
        //to load the configObj value after save 
        configObj = JSON.parse(JSON.stringify(rawconfigObj));
		/*dialog.showSaveDialog({
			title : 'Save Video As',
			defaultPath : optname,
			buttonLabel : 'Save Video',
			filters : [
				{	name : 'Video' , extensions : ['mp4']	}
			]
		},function(filename)
		{
			
			if(filename != undefined)
			{
				
				
				//rename backup folder for mode now
				console.log('selected path : ',filename,' output file : ',optname)
				var temp = __dirname.replace("\\process-window","")
				
				//console.log("inside if"+optname)
				
				console.log("rename from : ",temp+"\\"+(optname).split("/")[0])
				//console.log('rename to : ',temp+"\\"+((filename.split("\\"))[]))
				//fs.copyFileSync(optname,filename)
				//fs.unlinkSync(optname)
				finalFilePath = filename
				console.log(configObj.finalMerger[0].type)
				if(configObj.finalMerger[0].type == 'camera')
				{
					//var camFileName = fs.readdirSync(configFilePath+'/camera/')[0]
					//fs.copyFileSync(configFilePath+'/camera/'+camFileName,filename)
					//console.log(configFilePath+'/camera/'+camFileName,filename)
					alert('File Generated - '+configFilePath+configObj.finalMerger[0].cpath)
				}
				else if(configObj.finalMerger[0].type == 'screen')
				{
					//var screenFileName = fs.readdirSync(configFilePath+'/screen/')[0]
					//fs.copyFileSync(configFilePath+'/screen/'+screenFileName,filename)
					//console.log(configFilePath+'/screen/'+camFileName,filename)
					//console.log('screen copied')
					alert('File Generated - '+configFilePath+configObj.finalMerger[0].cpath)
				}
				else if(configObj.finalMerger[0].type == 'both')
				{
					//var screenFileName = fs.readdirSync(configFilePath+'/screen/')[0]
					//fs.copyFileSync(configFilePath+'/screen/'+screenFileName,filename)
					//console.log(configFilePath+'/screen/'+camFileName,filename)
					//console.log('screen copied')
					alert('File Generated - '+configFilePath+configObj.finalMerger[0].cpath)
				}
			}
			else
			{
				if(configObj.finalMerger[0].type == 'camera' || configObj.finalMerger[0].type == 'both')
				{
					camFileName = fs.readdirSync(configFilePath+'/camera/')[0]
					console.log(configFilePath+'/camera/'+camFileName,filename)
					fs.copyFileSync(configFilePath+'/camera/'+camFileName,configFilePath+'/output.mp4')
				}
				else if(configObj.finalMerger[0].type == 'screen')
				{
					screenFileName = fs.readdirSync(configFilePath+'/screen/')[0]
					console.log(configFilePath+'/camera/'+camFileName,filename)
					fs.copyFileSync(configFilePath+'/screen/'+screenFileName,configFilePath+'/output.mp4')
				}
				console.log('final file path set as : ')
				finalFilePath = (__dirname).replace("process-window",configFilePath+"\\output.mp4")
				//finalFilePath = (__dirname+'/'+configFilePath)
			}

			//fetchBackupFolders()
			
			if(fs.existsSync(configFilePath+'/mergingList.txt'))
			{
				fs.unlinkSync(configFilePath+'/mergingList.txt')	
			}
			$("#progress-text").text("")
			$(".progress").hide()
		})*/
    }
    else {
		/*var mergeArgs = [
			'ffmpeg',
			'-f' , 'concat',
			'-safe' , '0',
			'-i', configFilePath+'/mergingList.txt',
			'-s' , '1280x720',
			'-c:v' , 'libx264',
			'-c:a' , 'aac',
			'-crf' , configObj.crf,
			'-preset' , configObj.preset,
			'-y',optname,
		];*/
        var mergeArgs = getMP4Args(configFilePath)
        console.log(mergeArgs)
        var cmdArgs = mergeArgs.shift(); progress_bar_processing = a_progress_bar
        progress_bar_processing = progress_bar_processing.replace("file_name_here", "Merge files...");
        progress_bar_processing = progress_bar_processing.replace("id_here", "mergevideos");
        $(".progress-bar-container").append(progress_bar_processing)

        procArgs = child_process.spawn(cmdArgs, mergeArgs);
        ipcRenderer.send('pid-message', procArgs.pid);
        procArgs.on('close', function () {
            // if flagCancel==1 then process is cancel
            if (flagCancel != 1) {
                $('#mergevideos').html("<span>Merge Complete!</span>");
                $('#mergevideos').css('width', 100 + '%')
                console.log("Mergedone!")
                $("#progress-text").text("Video generated successfully !")
                $(".progress").hide()
                $(".show-file-wrap").show()
                $(".back-btn").show()
                ipcRenderer.send('clear-for-both');

                dialog.showSaveDialog({
                    title: 'Save Video As',
                    defaultPath: optname,
                    buttonLabel: 'Save Video',
                    filters: [
                        { name: 'Video', extensions: ['mp4'] }
                    ]
                }, function (filename) {
                    if (filename != undefined) {
                        console.log('selected path : ', filename, ' output file : ', optname)
                        fs.copyFileSync(optname, filename)
                        fs.unlinkSync(optname)
                        finalFilePath = filename

                        console.log(__dirname.replace("\\resources\\app.asar\\", "\\") + (optname).split("/")[0])
                    }
                    else {
                        finalFilePath = (__dirname).replace("process-window", configFilePath + "\\" + configFilePath + '.mp4')

                    }
                    //to load the configObj value after save 
                    configObj = JSON.parse(JSON.stringify(rawconfigObj));
                    //console.log(configObj,rawconfigObj)
                    // rimraf.sync(configFilePath+'/camera/')
                    // rimraf.sync(configFilePath+'/screen/')
                    $("#progress-text").text("")
                    $(".progress").hide()

                    swal({
                        title: "Video Processing Done !",
                        text: "Click to close button!",
                        icon: "success",
                        button: "Close!",
                    })
                    //remove progress-bar divs
                    remove_Progress_div();
                    //hide modal after processing done! 
                    $('#saveModal').modal('hide')
                    $('.modal-backdrop').hide()

                })
            }
        })
        procArgs.stderr.on('data', (data) => {

            $("#the-log").append("<div class='a-log'>" + data.toString() + "</div>")
            $("#the-log").scrollTop($("#the-log").prop("scrollHeight"))
            console.log(data.toString())
            $("#the-log").append("<div class='a-log'>" + data.toString() + "</div>")
            if (data.toString().includes("time=")) {
                let x1 = ((data.toString().split("time=")[1]).split(" ")[0]).split(":")
                let hrs = parseInt(x1[0])
                let mins = parseInt(x1[1])
                let secs = parseFloat(parseFloat(x1[2]).toFixed(2))
                doneDuration = (hrs * 60) + (mins * 60) + secs
                progress_width_merge = (((doneDuration / totalTime) * 100).toFixed(0))
                $('#mergevideos').css('width', progress_width_merge + '%')
                $('#mergevideos').html("<span>" + progress_width_merge + "%</span>");
                console.log('#mergevideos')
                console.log(progress_width_merge)

                $("#progress-text").html("Packing video together ...")
                $("#rec-progress-bar").css('width', ((doneDuration / totalDuration) * 100).toFixed(0) + '%')
            }
        })
        procArgs.on('err', function (err) {
            alert(err);
        })
    }
}
function getMP4Args(configFilePath) {
    //var drawTextObj = getDrawTextObj()
    var finalOverlay = getFinalOverlay()
    var inputFilesStr = getMergeInputFileStr(configFilePath)
    var filterComplex
    console.log('final overlay in getMP4Args: ', finalOverlay)
    filterComplex = getMergeFilterComplex(finalOverlay.finalOverlayText)
	/*if(drawTextObj.drawText.length == 0)
	{
		drawTextObj.lastLabel = 'v'
		filterComplex = getMergeFilterComplex()
	}
	else
	{
		filterComplex = getMergeFilterComplex(drawTextObj.drawText)
	}*/


    var arr = ['ffmpeg']
    arr = arr.concat(inputFilesStr)
    arr = arr.concat(['-filter_complex', filterComplex, '-map', '[' + finalOverlay.lastLabel + ']', '-map', '[a]', '-y', optname])
    return arr
}
function getFinalOverlay() {
    var finalOverlayRaw = '[label_in]scale=width_here:height_here [scaled_i_here],[last_label_here][scaled_i_here]overlay=left_here:top_here [label_out];'
    var finalOverlayCounter = 1
    var extFileStreamCounter = configObj.finalMerger.length
    var lastLabel = "v"
    var finalOverlayText = ""
    let background = {
        top: $("#background").prop("offsetTop"),
        left: $("#background").prop("offsetLeft"),
        width: $("#background").prop("offsetWidth"),
        height: $("#background").prop("offsetHeight"),
    }
    $('canvas').each(function (index, html) {
        if (this.getAttribute('canvas_file_type') == 'external-file') {
            console.log('overlay item :', $(this).parent())

            var temp = finalOverlayRaw

            temp = temp.replace(/label_in/g, extFileStreamCounter)

            let width = $(this).parent().css('width')
            let height = $(this).parent().css('height')
            let left = $(this).parent().css('left')
            let top = $(this).parent().css('top')

            let widthPer = parseInt((parseInt((width).substr(0, width.length - 2)) / background.width) * 100) + 1
            let heightPer = parseInt((parseInt((height).substr(0, height.length - 2)) / background.height) * 100) + 1
            let leftPer = parseInt((parseInt(left.substr(0, left.length - 2)) / background.width) * 100)
            let topPer = parseInt((parseInt(top.substr(0, top.length - 2)) / background.height) * 100)

            temp = temp.replace('width_here', widthPer * 12.8)
            temp = temp.replace('height_here', heightPer * 7.2)
            temp = temp.replace(/scaled_i_here/g, 'scaled_' + finalOverlayCounter)
            temp = temp.replace('last_label_here', lastLabel)
            temp = temp.replace('top_here', topPer * 7.2)
            temp = temp.replace('left_here', leftPer * 12.8)
            temp = temp.replace('label_out', 'new_overlay' + finalOverlayCounter)

            lastLabel = 'new_overlay' + finalOverlayCounter
            finalOverlayText += temp
            extFileStreamCounter++
            finalOverlayCounter++
            console.log(this, ' needs to be overlaid')
        }
    })
    //finalOverlayText = finalOverlayText.replace('[label_in]','[v]')
    finalOverlayText = finalOverlayText.substr(0, finalOverlayText.length - 1)

    console.log('final overlay text : ', finalOverlayText)
    return {
        finalOverlayText: finalOverlayText,
        lastLabel: lastLabel
    }
}
function getDrawTextObj() {
    var drawTextRaw = '[label_in]drawtext=fontfile=OpenSans-Regular.ttf: text=text_here: fontcolor=royalblue: fontsize=26: box=1: boxcolor=white@0.1: x=left_here:y=top_here [label_out];'
    var drawText = ""
    var lastLabel = ""
    var textOverlayCounter = 0
    $('.text-overlay').each(function (index, html) {
        textOverlayCounter++
        var temp = drawTextRaw
        if (textOverlayCounter != 1) {
            temp = temp.replace('label_in', lastLabel)
        }
        temp = temp.replace('text_here', this.innerText)
        temp = temp.replace('top_here', $(this).css('top').split('px')[0])
        temp = temp.replace('left_here', $(this).css('left').split('px')[0])
        temp = temp.replace('label_out', 'video' + textOverlayCounter)

        drawText += temp

        lastLabel = 'video' + textOverlayCounter
        console.log($(this).css('left'))
    })
    drawText = drawText.replace('[label_in]', '')
    drawText = drawText.substr(0, drawText.length - 1)
    if (drawText.length == 0) {
        return {
            drawText: '',
            lastLabel: 'v'
        }
    }
    else {
        return {
            drawText: drawText,
            lastLabel: lastLabel
        }
    }
}
function getMergeInputFileStr(configFilePath) {
    var filesStr = []
    var theFiles = filesJs.getTheFiles()
    for (let i = 0; i < (configObj.finalMerger.length); i++) {

        var xx = configObj.finalMerger[i].cpath;
        var txt = (xx.split("/")[xx.split("/").length - 2]) + "/" + (xx.split("/")[xx.split("/").length - 1])
        txt = txt.replace('/', '\\')
		/*filesStr += "-i "+(txt.trim('\n')+' ')
		filesStr += "-i "+xx+" "*/
        filesStr.push('-i')
        filesStr.push(configFilePath + '\\' + (txt.trim('\n') + ' '))
    }
    console.log('fileStr : ', filesStr)

    $('canvas').each(function (index, html) {
        if (this.getAttribute('canvas_file_type') == 'external-file') {
            var id = 'file-' + this.id.split('-')[1]
            console.log('file path : ', theFiles[id], 'id : ', id)
            filesStr.push('-i')
            filesStr.push(theFiles[id].filePath[0])
        }
    })
    return filesStr
}
function getMergeFilterComplex(finalOverlayText) {
    var filterComplex = ''
    console.log('finalOverlayText ->', finalOverlayText, '<-')
    for (var i = 0; i < (configObj.finalMerger.length); i++) {
        filterComplex += '[' + i + ':v] [' + i + ':a] '
    }
    if (finalOverlayText == '') {
        filterComplex += 'concat=n=' + (configObj.finalMerger.length) + ':v=1:a=1 [v] [a]'
    }
    else {
        filterComplex += 'concat=n=' + (configObj.finalMerger.length) + ':v=1:a=1 [v] [a];'
        filterComplex += finalOverlayText
    }
    return filterComplex
}

/*The function is not used anywhere*/
function replaceBeforeOverlay(configFilePath) {
    var newFiles = [], oldFiles = []
    console.log(configFilePath)
    fs.readdirSync(configFilePath + '/camera').forEach(file => {
        if (file.includes("new_"))
            newFiles.push(configFilePath + '/camera/' + file)
        else {
            oldFiles.push(configFilePath + '/camera/' + file)
        }
    })
    for (let i = 0; i < newFiles.length; i++) {
        let x = newFiles[i].split("_")[newFiles[i].split("_").length - 1];
        for (let j = 0; j < oldFiles.length; j++) {
            if (oldFiles[j].includes(x)) {
                fs.unlinkSync(oldFiles[j])
                fs.renameSync(newFiles[i], oldFiles[j])
            }
        }
    }
    console.log('new : ', newFiles, 'old : ', oldFiles)
    newFiles = []
    oldFiles = []
    fs.readdirSync(configFilePath + '/screen').forEach(file => {
        if (file.includes("new_"))
            newFiles.push(configFilePath + '/screen/' + file)
        else {
            oldFiles.push(configFilePath + '/screen/' + file)
        }
    })
    console.log('new : ', newFiles, 'old : ', oldFiles)
    for (let i = 0; i < newFiles.length; i++) {
        let x = newFiles[i].split("_")[newFiles[i].split("_").length - 1];
        for (let j = 0; j < oldFiles.length; j++) {
            if (oldFiles[j].includes(x)) {
                fs.unlinkSync(oldFiles[j])
                fs.renameSync(newFiles[i], oldFiles[j])
            }
        }
    }
}

function generateVideo(canvases, background, cb) {
    console.log('canvases : ', canvases, 'background : ', background)
    canvases.sort(function (a, b) {
        return a.z_index - b.z_index
    })

    canvases.each(index, obj)
    {

    }

    cb("completed")
}
function cutSelectedArea(areaSelectDivObj, cb) {
    console.log(areaSelectDivObj)
}
function sortObject(obj) {
    var temp = []
    var tempObj = {}
    for (var key in obj) {
        temp.push(obj[key])
    }
    temp.sort(function (a, b) {
        return a.left - b.left
    })
    console.log('sorted obj : ', temp)
    return temp
}
function updateConfigObject(configObject, canvases, areaObj) {
    console.log(areaObj)
    var newAreaObjArr = sortObject(areaObj)
    configObject.cutArea = [];

    console.log(newAreaObjArr)
    for (var i = 0; i < newAreaObjArr.length; i++) {
        configObject.cutArea.push({
            start: newAreaObjArr[i].left / 10,
            end: newAreaObjArr[i].right / 10,
            duration: newAreaObjArr[i].width / 10,
        });
    }
    console.log(configObject.cutArea)

    for (let i = 0; i < configObject.finalMerger.length; i++) {
        let obj = configObject.finalMerger[i]

        if (obj.type == 'camera') {
            let temp;
            let tempid = "canvas-" + (i + 1);

            for (let j = 0; j < canvases.length; j++) {

                if (canvases[j].id == tempid) {
                    temp = j;
                }
            }


            if (canvases[temp].widthper > 100) {
                configObject.finalMerger[i].cwidthper = 100;
            }
            else {
                configObject.finalMerger[i].cwidthper = canvases[temp].widthper;
            }
            if (canvases[temp].heightper > 100) {
                configObject.finalMerger[i].cheightper = 100;
            }
            else {
                configObject.finalMerger[i].cheightper = canvases[temp].heightper;
            }
            if (isNaN(canvases[temp].leftper)) {
                configObject.finalMerger[i].cleftper = 0;
            }
            else {
                configObject.finalMerger[i].cleftper = canvases[temp].leftper;
            }

            if (isNaN(canvases[temp].topper)) {
                configObject.finalMerger[i].ctopper = 0;
            }
            else {
                configObject.finalMerger[i].ctopper = canvases[temp].topper;
            }



            if ((configObject.finalMerger[i].cwidth = parseInt(canvases[temp].widthper * 12.8)) % 2 != 0) {
                configObject.finalMerger[i].cwidth += 1;
            }


            if ((configObject.finalMerger[i].cheight = parseInt(configObject.finalMerger[i].cheightper * 7.2)) % 2 != 0) {
                configObject.finalMerger[i].cheight += 1;
            }


            if (isNaN(canvases[temp].left)) {
                configObject.finalMerger[i].cleft = 0;
            }
            else {
                configObject.finalMerger[i].cleft = parseInt(configObject.finalMerger[i].cleftper * 12.8);
            }

            if (isNaN(canvases[temp].top)) {
                configObject.finalMerger[i].ctop = 0;
            }
            else {
                configObject.finalMerger[i].ctop = parseInt(configObject.finalMerger[i].ctopper * 7.2);
            }


            configObject.finalMerger[i].cz_index = canvases[temp].z_index;
            configObject.finalMerger[i].cid = canvases[temp].id;
            configObject.finalMerger[i].padding = canvases[temp].padding;


        }



        else if (obj.type == 'screen') {
            let temp;
            let tempid = "canvas-" + (i + 1);

            for (let j = 0; j < canvases.length; j++) {
                if (canvases[j].id == tempid) {
                    temp = j;
                }
            }

            configObject.finalMerger[i].swidthper = canvases[temp].widthper;

            if (canvases[temp].heightper > 100) {
                configObject.finalMerger[i].sheightper = 100;
            }
            else {
                configObject.finalMerger[i].sheightper = canvases[temp].heightper;
            }




            if ((configObject.finalMerger[i].swidth = parseInt(canvases[temp].widthper * 12.8)) % 2 != 0) {
                configObject.finalMerger[i].swidth += 1;
            }


            if ((configObject.finalMerger[i].sheight = parseInt(configObject.finalMerger[i].sheightper * 7.2)) % 2 != 0) {
                configObject.finalMerger[i].sheight += 1;
            }


            if (isNaN(canvases[temp].leftper)) {
                configObject.finalMerger[i].sleftper = 0;
            }
            else {
                configObject.finalMerger[i].sleftper = canvases[temp].leftper;
            }

            if (isNaN(canvases[temp].topper)) {
                configObject.finalMerger[i].stopper = 0;
            }
            else {
                configObject.finalMerger[i].stopper = canvases[temp].topper;
            }






            if (isNaN(canvases[temp].left)) {
                configObject.finalMerger[i].sleft = 0;
            }
            else {
                configObject.finalMerger[i].sleft = parseInt(configObject.finalMerger[i].sleftper * 12.8);
            }

            if (isNaN(canvases[temp].top)) {
                configObject.finalMerger[i].stop = 0;
            }
            else {
                configObject.finalMerger[i].stop = parseInt(configObject.finalMerger[i].stopper * 7.2);
            }








            configObject.finalMerger[i].sz_index = canvases[temp].z_index;
            configObject.finalMerger[i].sid = canvases[temp].id;
            configObject.finalMerger[i].padding = canvases[temp].padding;


        }
        else if (obj.type == 'both') {
            let temp;
            let tempid = "canvas-" + (i + 1) + "-1";

            for (let j = 0; j < canvases.length; j++) {
                if (canvases[j].id == tempid) {
                    temp = j;
                }
            }

            if (canvases[temp].type == 'both-screen') {

                configObject.finalMerger[i].swidthper = canvases[temp].widthper;

                if (canvases[temp].heightper > 100) {
                    configObject.finalMerger[i].sheightper = 100;
                }
                else {
                    configObject.finalMerger[i].sheightper = canvases[temp].heightper;
                }


                if ((configObject.finalMerger[i].swidth = parseInt(canvases[temp].widthper * 12.8)) % 2 != 0) {
                    configObject.finalMerger[i].swidth += 1;
                }


                if ((configObject.finalMerger[i].sheight = parseInt(configObject.finalMerger[i].sheightper * 7.2)) % 2 != 0) {
                    configObject.finalMerger[i].sheight += 1;
                }


                if (isNaN(canvases[temp].leftper)) {
                    configObject.finalMerger[i].sleftper = 0;
                }
                else {
                    configObject.finalMerger[i].sleftper = canvases[temp].leftper;
                }

                if (isNaN(canvases[temp].topper)) {
                    configObject.finalMerger[i].stopper = 0;
                }
                else {
                    configObject.finalMerger[i].stopper = canvases[temp].topper;
                }




                if (isNaN(canvases[temp].left)) {
                    configObject.finalMerger[i].sleft = 0;
                }
                else {
                    configObject.finalMerger[i].sleft = parseInt(configObject.finalMerger[i].sleftper * 12.8);
                }

                if (isNaN(canvases[temp].top)) {
                    configObject.finalMerger[i].stop = 0;
                }
                else {
                    configObject.finalMerger[i].stop = parseInt(configObject.finalMerger[i].stopper * 7.2);
                }






                configObject.finalMerger[i].sz_index = canvases[temp].z_index;
                configObject.finalMerger[i].sid = canvases[temp].id;
                configObject.finalMerger[i].spadding = canvases[temp].padding;

            }
            else //both-camera
            {

                configObject.finalMerger[i].cwidthper = canvases[temp].widthper;
                if (canvases[temp].heightper > 100) {
                    configObject.finalMerger[i].cheightper = 100;
                }
                else {
                    configObject.finalMerger[i].cheightper = canvases[temp].heightper;
                }


                if ((configObject.finalMerger[i].cwidth = parseInt(canvases[temp].widthper * 12.8)) % 2 != 0) {
                    configObject.finalMerger[i].cwidth += 1;
                }


                if ((configObject.finalMerger[i].cheight = parseInt(configObject.finalMerger[i].cheightper * 7.2)) % 2 != 0) {
                    configObject.finalMerger[i].cheight += 1;
                }


                if (isNaN(canvases[temp].leftper)) {
                    configObject.finalMerger[i].cleftper = 0;
                }
                else {
                    configObject.finalMerger[i].cleftper = canvases[temp].leftper;
                }

                if (isNaN(canvases[temp].topper)) {
                    configObject.finalMerger[i].ctopper = 0;
                }
                else {
                    configObject.finalMerger[i].ctopper = canvases[temp].topper;
                }



                if (isNaN(canvases[temp].left)) {
                    configObject.finalMerger[i].cleft = 0;
                }
                else {
                    configObject.finalMerger[i].cleft = parseInt(configObject.finalMerger[i].cleftper * 12.8);
                }

                if (isNaN(canvases[temp].top)) {
                    configObject.finalMerger[i].ctop = 0;
                }
                else {
                    configObject.finalMerger[i].ctop = parseInt(configObject.finalMerger[i].ctopper * 7.2);
                }



                configObject.finalMerger[i].cz_index = canvases[temp].z_index;
                configObject.finalMerger[i].cid = canvases[temp].id;
                configObject.finalMerger[i].cpadding = canvases[temp].cadding;

            }


            tempid = "canvas-" + (i + 1) + "-2";

            for (let j = 0; j < canvases.length; j++) {
                if (canvases[j].id == tempid) {
                    temp = j;
                }
            }

            if (canvases[temp].type == 'both-screen') {
                configObject.finalMerger[i].swidthper = canvases[temp].widthper;
                if (canvases[temp].heightper > 100) {
                    configObject.finalMerger[i].cheightper = 100;
                }
                else {
                    configObject.finalMerger[i].cheightper = canvases[temp].heightper;
                }

                if ((configObject.finalMerger[i].swidth = parseInt(canvases[temp].widthper * 12.8)) % 2 != 0) {
                    configObject.finalMerger[i].swidth += 1;
                }


                if ((configObject.finalMerger[i].sheight = parseInt(configObject.finalMerger[i].cheightper * 7.2)) % 2 != 0) {
                    configObject.finalMerger[i].sheight += 1;
                }


                if (isNaN(canvases[temp].leftper)) {
                    configObject.finalMerger[i].sleftper = 0;
                }
                else {
                    configObject.finalMerger[i].sleftper = canvases[temp].leftper;
                }

                if (isNaN(canvases[temp].topper)) {
                    configObject.finalMerger[i].stopper = 0;
                }
                else {
                    configObject.finalMerger[i].stopper = canvases[temp].topper;
                }




                if (isNaN(canvases[temp].left)) {
                    configObject.finalMerger[i].sleft = 0;
                }
                else {
                    configObject.finalMerger[i].sleft = parseInt(configObject.finalMerger[i].sleftper * 12.8);
                }

                if (isNaN(canvases[temp].top)) {
                    configObject.finalMerger[i].stop = 0;
                }
                else {
                    configObject.finalMerger[i].stop = parseInt(configObject.finalMerger[i].stopper * 7.2);
                }






                configObject.finalMerger[i].sz_index = canvases[temp].z_index;
                configObject.finalMerger[i].sid = canvases[temp].id;
                configObject.finalMerger[i].spadding = canvases[temp].padding;

            }
            else {
                configObject.finalMerger[i].cwidthper = canvases[temp].widthper;
                if (canvases[temp].heightper > 100) {
                    configObject.finalMerger[i].cheightper = 100;
                }
                else {
                    configObject.finalMerger[i].cheightper = canvases[temp].heightper;
                }

                if ((configObject.finalMerger[i].cwidth = parseInt(canvases[temp].widthper * 12.8)) % 2 != 0) {
                    configObject.finalMerger[i].cwidth += 1;
                }


                if ((configObject.finalMerger[i].cheight = parseInt(configObject.finalMerger[i].cheightper * 7.2)) % 2 != 0) {
                    configObject.finalMerger[i].cheight += 1;
                }



                if (isNaN(canvases[temp].leftper)) {
                    configObject.finalMerger[i].cleftper = 0;
                }
                else {
                    configObject.finalMerger[i].cleftper = canvases[temp].leftper;
                }

                if (isNaN(canvases[temp].topper)) {
                    configObject.finalMerger[i].ctopper = 0;
                }
                else {
                    configObject.finalMerger[i].ctopper = canvases[temp].topper;
                }



                if (isNaN(canvases[temp].left)) {
                    configObject.finalMerger[i].cleft = 0;
                }
                else {
                    configObject.finalMerger[i].cleft = parseInt(configObject.finalMerger[i].cleftper * 12.8);
                }

                if (isNaN(canvases[temp].top)) {
                    configObject.finalMerger[i].ctop = 0;
                }
                else {
                    configObject.finalMerger[i].ctop = parseInt(configObject.finalMerger[i].ctopper * 7.2);
                }



                configObject.finalMerger[i].cz_index = canvases[temp].z_index;
                configObject.finalMerger[i].cid = canvases[temp].id;
                configObject.finalMerger[i].cpadding = canvases[temp].padding;
            }

        }

    }

    console.log(configObject);
    return configObject
}
function killChildProcess() {

    flagCancel = 1
    console.log(processes)
    for (let i = 0; i < processes.length; i++) {
        console.log(processes[i].killed)
        processes[i].kill('SIGINT')
        console.log(processes[i].killed + " Preprocessing killed ..................")
    }
    console.log(processes)
    console.log(procO)
    if (procO != undefined) {
        procO.kill('SIGINT')
        console.log(procO)
        console.log("Overlay kill")
    }
    console.log("Overlay kill after")
    console.log(procArgs)
    if (procArgs != undefined) {
        console.log(procArgs)
        procArgs.kill('SIGINT')
        console.log("Merge kill")
    }
    console.log("Merge kill after")
    console.log(configObj, "old configObj")
    //new config create after cancel

    // console.log(configObj, "New configObj")
    //remove progress-bar divs
    // delete configObj;
    remove_Progress_div();

}
function remove_Progress_div() {
    $('.progress-bar-content').each(function () {
        var current_element = $(this);
        console.log("element", current_element)
        console.log("parent", current_element.parent())
        current_element.remove();
        console.log("remove count..")
    })

}

function getDivBy2(val) {
    console.log(val)
    if (val % 2 != 0) {
        console.log('value set as :  ', (val + 1));
        return (val + 1);
    }
    else {
        console.log('value set as ', val)
        return val;
    }
}
module.exports = {
    generateVideo: generateVideo,
    cutSelectedArea: cutSelectedArea,
    preprocess: preprocess,
    killChildProcess: killChildProcess,
    remove_Progress_div: remove_Progress_div
};
